# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-17 15:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticket', '0022_auto_20170817_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='type',
            field=models.CharField(choices=[('created', 'Created')], default='created', max_length=100),
        ),
    ]
