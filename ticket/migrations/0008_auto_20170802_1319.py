# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-02 13:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticket', '0007_ticket_category'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ticket',
            name='category',
        ),
        migrations.AddField(
            model_name='ticket',
            name='type',
            field=models.CharField(choices=[('development', 'Development'), ('information', 'Information'), ('install/setup', 'Install / Setup'), ('issue', 'Issue'), ('licensing', 'Licensing'), ('question', 'Question'), ('rma', 'RMA'), ('upgrade', 'Upgrade')], default='issue', max_length=100),
        ),
    ]
