# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-21 11:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ticket', '0045_auto_20170921_1157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='company',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='company', to='ticket.Company'),
        ),
    ]
