import datetime
from haystack import indexes
from ticket.models import Ticket, Comment


class TicketIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    description = indexes.CharField(model_attr='description')
    # updated = indexes.DateTimeField(model_attr='updated')

    def get_model(self):
        return Ticket

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # .objects.filter(updated__lte=datetime.datetime.now())


class CommentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    body = indexes.CharField(model_attr='body')
    # updated = indexes.DateTimeField(model_attr='updated')

    def get_model(self):
        return Comment

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all() # .objects.filter(updated__lte=datetime.datetime.now())