# Event types ---------------------------------
COMMENT_EVENT = 'comment'
CREATED_EVENT = 'created'
DELETED_EVENT = 'deleted'
SUBSCRIBED_EVENT = 'subscribed'
UPDATED_EVENT = 'updated'
UNSUBSCRIBED_EVENT = 'unsubscribed'

EVENT_TYPES = (
    (COMMENT_EVENT, 'Comment'),
    (CREATED_EVENT, 'Created'),
    (DELETED_EVENT, 'Deleted'),
    (SUBSCRIBED_EVENT, 'Subscribed'),
    (UPDATED_EVENT, 'Updated'),
    (UNSUBSCRIBED_EVENT, 'Unsubscribed'),
)
# Event types ---------------------------------

# Priorities ----------------------------------
CRITICAL_PRIORITY = 'critical'
HIGH_PRIORITY = 'high'
MED_PRIORITY = 'medium'
LOW_PRIORITY = 'low'

PRIORITIES = (
    (CRITICAL_PRIORITY, 'Critical'),
    (HIGH_PRIORITY, 'High'),
    (MED_PRIORITY, 'Medium'),
    (LOW_PRIORITY, 'Low'),
)
# Priorities ----------------------------------

# Statuses ------------------------------------
AWAITING_DEV = 'Awaiting Dev'
OPEN = 'Open'
CLOSED = 'Closed'
ON_HOLD = 'On Hold'
RESOLVED = 'Resolved'
TO_TEST = 'To Test'

STATUSES = (
    (AWAITING_DEV, 'Awaiting Dev'),
    (OPEN, 'Open'),
    (CLOSED, 'Closed'),
    (ON_HOLD, 'On Hold'),
    (RESOLVED, 'Resolved'),
    (TO_TEST, 'To Test'),
)
# Statuses ------------------------------------

# Types ---------------------------------------
DEV_TYPE = 'development'
INFO_TYPE = 'information'
INSTALL_SETUP_TYPE = 'install/setup'
ISSUE_TYPE = 'issue'
LICENSING_TYPE = 'licensing'
QUESTION_TYPE = 'question'
RMA_TYPE = 'rma'
UPGRADE_TYPE = 'upgrade'

TYPES = (
    (DEV_TYPE, 'Development'),
    (INFO_TYPE, 'Information'),
    (INSTALL_SETUP_TYPE, 'Install / Setup'),
    (ISSUE_TYPE, 'Issue'),
    (LICENSING_TYPE, 'Licensing'),
    (QUESTION_TYPE, 'Question'),
    (RMA_TYPE, 'RMA'),
    (UPGRADE_TYPE, 'Upgrade'),
)
# Types ---------------------------------------
