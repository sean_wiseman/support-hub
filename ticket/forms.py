from django.contrib.auth.models import User
from django import forms
from typing import List
from ticket.models import (
    Comment,
    Company,
    EmailSubscriber,
    FileAttachment,
    Subscriber,
    Tag,
    Ticket
)


class CommentCreateForm(forms.Form):
    body = forms.CharField(widget=forms.Textarea)
    internal = forms.BooleanField(required=False)
    owner = forms.IntegerField()
    ticket = forms.IntegerField()
    attachment = forms.FileField(
        label='Attach a file',
        required=False
    )

    def save(self, commit=True):
        body = self.cleaned_data['body']
        internal = self.cleaned_data['internal']
        owner_id = self.cleaned_data['owner']
        ticket_id = self.cleaned_data['ticket']
        attachment = self.cleaned_data['attachment']

        owner = User.objects.get(id=owner_id)
        ticket = Ticket.objects.get(id=ticket_id)

        self.instance = Comment.objects.create(
            body=body,
            internal=internal,
            owner=owner,
            ticket=ticket
        )

        if attachment:
            FileAttachment.objects.create(
                comment=self.instance,
                raw_file=attachment
            )

        return True

# class CommentCreateForm(forms.ModelForm):
#     class Meta:
#         model = Comment
#         fields = (
#             'body',
#             'internal',
#             'owner',
#             'ticket'
#         )


class EmailSubscriberForm(forms.ModelForm):
    class Meta:
        model = EmailSubscriber
        fields = (
            'address',
            'ticket'
        )


class SubscriberForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = (
            'owner',
            'ticket'
        )


class TagCreateAndAddForm(forms.Form):
    name = forms.CharField(max_length=250, required=True)
    ticket_id = forms.IntegerField()

    @staticmethod
    def parse_multi_name_data(data: str) -> List[str]:
        """Parses str data supplied by user, if comma seperated, will handle
        multiple name entries
        """
        return [tag.strip(' ').lower() for tag in data.split(',')]

    def save(self, commit=True):
        name_data = self.cleaned_data['name']
        ticket_id = self.cleaned_data['ticket_id']
        ticket = Ticket.objects.get(id=ticket_id)
        names = self.parse_multi_name_data(name_data)
        if commit:
            for name in names:
                tag, created = Tag.objects.get_or_create(name=name)
                ticket.tags.add(tag)
        return True


class TagRemoveForm(forms.Form):
    tag_id = forms.IntegerField()
    ticket_id = forms.IntegerField()


class TicketForm(forms.ModelForm):
    # accepted = forms.BooleanField(widget=forms.HiddenInput())
    # responded = forms.BooleanField(widget=forms.HiddenInput())
    # id = forms.IntegerField(widget=forms.HiddenInput())
    company = forms.ModelChoiceField(queryset=Company.objects.all(), to_field_name='name', required=False)

    class Meta:
        model = Ticket
        fields = (
            'title',
            'category',
            'type',
            'priority',
            'company',
            'status',
            'assigned_to',
            'site_name',
            'duration',
            'description',
        )

        # reported by == auto
        # tags only on edit
        # updated == auto on save
        # updated_by == auto

    def clean(self):
        return super(TicketForm, self).clean()

    def save(self, commit=True, reported_by_user=None, updated_by_user=None):
        """reported_by_user allows the form to be used outside of views.

        Simply pass it a User object when saving outside of a view:
        >> form = TicketCreateForm(data)
        >> form.save(reported_by_user=User.objects.first(), updated_by_user=User.objects.first())

        Used in a view the reported_by field is auto populated.

        """
        instance = super(TicketForm, self).save(commit=False)

        if reported_by_user:
            instance.reported_by = reported_by_user

        if updated_by_user:
            instance.updated_by = updated_by_user

        if commit:
            instance.save()
        return instance


class TicketUpdateForm(forms.ModelForm):

    class Meta:
        model = Ticket
        fields = (
            'title',
            'category',
            'type',
            'priority',
            'company',
            'status',
            'assigned_to',
            'site_name',
            'duration',
            'description',
        )

        # reported by == auto
        # tags only on edit
        # updated == auto on save
        # updated_by == auto

    def clean(self):
        return super(TicketUpdateForm, self).clean()

    def save(self, commit=True, reported_by_user=None, updated_by_user=None):
        """reported_by_user allows the form to be used outside of views.

        Simply pass it a User object when saving outside of a view:
        >> form = TicketCreateForm(data)
        >> form.save(reported_by_user=User.objects.first(), updated_by_user=User.objects.first())

        Used in a view the reported_by field is auto populated.

        """
        instance = super(TicketUpdateForm, self).save(commit=False)

        if reported_by_user:
            instance.reported_by = reported_by_user

        if updated_by_user:
            instance.updated_by = updated_by_user

        if commit:
            instance.save()
        return instance


class TicketMergeForm(forms.Form):
    from_ticket = forms.IntegerField()
    to_ticket = forms.IntegerField()


class TicketMultiMergeForm(forms.Form):
    to_ticket = forms.IntegerField()


class TicketReportingForm(forms.Form):
    company = forms.ModelChoiceField(
        queryset=Company.objects.all(),
        to_field_name='name',
        required=False,
        empty_label="All"
    )
    year = forms.IntegerField(required=False)
