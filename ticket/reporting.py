from graphos.sources.model import ModelDataSource
from graphos.sources.simple import SimpleDataSource
# from graphos.renderers.flot import BarChart, LineChart, PieChart
from graphos.renderers.gchart import BarChart, ColumnChart, LineChart, PieChart
from django.shortcuts import render_to_response
from ticket.models import Category, Company, Ticket
from ticket.ticketDefaults import TYPES as ticket_types
import datetime


class ReportGenerator(object):
    company_names = []

    def __init__(self, company=None, year=None):
        """
        >>> report = ReportGenerator(company, from_date, to_date).get_data()

        :param company:
        :param from_date:
        :param to_date:
        """
        self.company = company
        self.raw_year = year
        self.date_today = datetime.date.today()

    @property
    def all_company_names(self):
        """Returns a list of all company names from Company objects"""
        if not self.company_names:
            self.company_names = [c.name for c in Company.objects.all()]
        return self.company_names

    @property
    def company_name(self):
        if self.company:
            return self.company.name
        else:
            return ''

    @property
    def year(self):
        if self.raw_year:
            return self.raw_year
        else:
            return datetime.date.today().year

    def get_data(self):

        by_category_data = self.tickets_by_category_data()
        by_month_data = self.tickets_by_month_data()
        by_duration_data = self.tickets_by_duration_data()
        by_type_data = self.tickets_by_type_data()

        return {
            'all_company_names': self.all_company_names,
            'company': self.company_name,
            'year': self.year,
            'by_category': {
                'chart': ChartFactory.get_chart('bar', by_category_data, **self.get_default_options('By Category')),
                'table': by_category_data
            },
            'by_duration': {
                'chart': ChartFactory.get_chart(
                    'column',
                    by_duration_data,
                    **self.get_default_options('By Duration (minutes)')
                ),
                'table': by_duration_data
            },
            'by_month': {
                'chart': ChartFactory.get_chart('column', by_month_data, **self.get_default_options('By Month')),
                'table': by_month_data
            },
            'by_type': {
                'chart': ChartFactory.get_chart('line', by_type_data, **self.get_default_options('By Type')),
                'table': by_type_data
            }
        }

    @staticmethod
    def get_default_options(title=''):
        return {
            'width': '100%',
            'options': {'title': title}
        }

    def get_queryset(self):
        if self.company_name:
            return Ticket.objects.filter(company__name=self.company_name)
        else:
            return Ticket.objects.all()

    @property
    def month_base_data(self):
        return [
            ['month'],
            ['jan'],
            ['feb'],
            ['mar'],
            ['apr'],
            ['may'],
            ['jun'],
            ['jul'],
            ['aug'],
            ['sep'],
            ['oct'],
            ['nov'],
            ['dec']
        ]

    def tickets_by_month_data(self):
        by_month_data = self.month_base_data

        # process single company request
        if self.company_name:
            by_month_data[0].append(self.company_name)

            month_count = Ticket.reporting.by_month(company=self.company_name, year=self.year)
            for month in month_count:
                by_month_data[month].append(month_count[month])
        else:
            month_count = Ticket.reporting.by_month(year=self.year)
            by_month_data[0] += self.all_company_names

            for company in self.all_company_names:
                for month, count in month_count[company].items():
                    by_month_data[month].append(count)

        return by_month_data

    def tickets_by_category_data(self):

        if self.company_name:
            by_category_data = [['Category', self.company_name]]
            category_count = Ticket.reporting.by_category(company=self.company_name, year=self.year)
            for category, count in category_count.items():
                by_category_data.append([category, count])

        else:
            by_category_data = [['Category']]
            category_count = Ticket.reporting.by_category(year=self.year)

            # add company name
            for company in category_count:
                by_category_data[0].append(company)

            for category in Category.objects.all():
                if category:
                    sub_cat = [category.name]
                    for company in category_count:
                        sub_cat.append(category_count[company][category.name])
                    by_category_data.append(sub_cat)

        return by_category_data

    def tickets_by_duration_data(self):
        by_duration_data = self.month_base_data

        # process single company request
        if self.company_name:
            by_duration_data[0].append(self.company_name)
            duration_count = Ticket.reporting.duration_by_month(company=self.company_name, year=self.year)

            for month, count in duration_count.items():
                by_duration_data[month].append(count)

        else:
            duration_count = Ticket.reporting.duration_by_month(year=self.year)

            by_duration_data[0] += self.all_company_names

            for company in self.all_company_names:
                for month, count in duration_count[company].items():
                    by_duration_data[month].append(count)

        return by_duration_data

    def tickets_by_type_data(self):
        by_type_data = self.type_base_data

        # process single company request
        if self.company_name:
            by_type_data[0].append(self.company_name)

            type_count = Ticket.reporting.by_type(company=self.company_name, year=self.year)

            for i in range(1, len(ticket_types) + 1):
                by_type_data[i].append(type_count[by_type_data[i][0]])
        else:
            by_type_data[0] += self.all_company_names

            for company_index in range(1, len(self.all_company_names) + 1):
                type_count = {t[0]: 0 for t in ticket_types}

                # get by type counts
                for ticket in Ticket.objects\
                        .filter(company__name=self.all_company_names[company_index - 1])\
                        .filter(created__year=self.year):
                    type_count[ticket.type] += 1

                for i in range(1, len(ticket_types) + 1):
                    by_type_data[i].append(type_count[by_type_data[i][0]])

        return by_type_data

    def total_ticket_count(self, from_date=None, to_date=None):
        if self.company_name:
            # int for single company
            return Ticket.objects.filter(company__name=self.company_name).count()
        else:
            # list of dicts for all companies e.g. [{'2DB': 2}, {'some_company': 1}]
            return [{c: Ticket.objects.filter(company__name=c).count()} for c in self.all_company_names]

    @property
    def type_base_data(self):
        return [['type']] + [[t[0]] for t in ticket_types]


class ChartFactory(object):

    @staticmethod
    def get_chart(chart_type, data, **kwargs):
        charts = {
            'bar': BarChart,
            'column': ColumnChart,
            'line': LineChart,
            'pie': PieChart,
        }
        data = SimpleDataSource(data=data)

        return charts[chart_type](data, **kwargs)
