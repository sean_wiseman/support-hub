from django.contrib import admin
from ticket.models import (
    AdminSubscriber,
    Category,
    Comment,
    Company,
    EmailSubscriber,
    FileAttachment,
    Tag,
    Ticket,
    Subscriber
)


class TicketAdmin(admin.ModelAdmin):
    model = Ticket
    search_fields = ('id', 'title')
    list_display = (
        'id',
        'title',
        'company',
        'created',
        'status',
        'category',
        'type',
        'assigned_to',
        'updated_by'
    )


admin.site.register(AdminSubscriber)
admin.site.register(Category)
admin.site.register(Comment)
admin.site.register(Company)
admin.site.register(EmailSubscriber)
admin.site.register(FileAttachment)
admin.site.register(Subscriber)
admin.site.register(Tag)
admin.site.register(Ticket, TicketAdmin)
