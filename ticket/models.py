from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage, BadHeaderError
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django_mailbox.signals import message_received
from django_mailbox.models import Message
from dirtyfields import DirtyFieldsMixin
from typing import Any, Dict, List
import ticket.ticketDefaults as td
import logging
import os
import datetime


# get reference to user model
# User = settings.AUTH_USER_MODEL


class DirtyFieldsGrabberMixin(DirtyFieldsMixin):

    def check_if_field_dirty(self, field: str) -> Dict[str, Any]:
        result = {
            'changed': False,
            'field': field,
            'saved': None,
            'current': None,
        }

        d_fields = self.get_dirty_fields(verbose=True, check_relationship=True)

        try:
            if d_fields:
                changed_field = d_fields.get(field, None)
                if changed_field and changed_field.get('saved', None):
                    result['changed'] = True
                    result['saved'] = changed_field['saved']
                    result['current'] = changed_field['current']
        except AttributeError as err:
            self.logger.error(
                '%(err_type)s: %(err)s - unable to check_if_field_dirty()',
                {
                    'err_type': type(err),
                    'err': err
                }
            )

        return result

    def get_changed_fields_detail(self) -> List[str]:
        ignore_fields = ['updated']

        if self.is_dirty():
            df = self.get_foreign_object_details(self.get_dirty_fields(verbose=True, check_relationship=True))
            return ['{0}: {1} -> {2}'.format(f, df[f]['saved'], df[f]['current']) for f in df if f not in ignore_fields]

    @staticmethod
    def get_foreign_object_details(data):
        """
        Get object details and replace pk in string repl e.g. company: 1 to company: '2DB'
        data = e.g. {
            'category': {'saved': 3, 'current': 2},
            'assigned_to': {'saved': 2, 'current': 1}
        }
        """
        objs = {
            'assigned_to': {'class': User, 'field': 'username'},
            'category': {'class': Category, 'field': 'name'},
            'company': {'class': Company, 'field': 'name'}
        }
        for field in data:
            if field in objs:
                if data[field]['saved']:
                    data[field]['saved'] = objs[field]['class'].objects.get(pk=data[field]['saved'])
                if data[field]['current']:
                    data[field]['current'] = objs[field]['class'].objects.get(pk=data[field]['current'])

        return data


class AdminSubscriber(models.Model):
    owner = models.ForeignKey(User, related_name='admin_subscriber_owner', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.owner)


class Category(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)


class Company(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return '{}'.format(self.name)


class Tag(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return '{}'.format(self.name)


class TicketReportingManager(models.Manager):

    def by_category(self, company='', year=datetime.date.today().year):
        """

        >>> by_category_data = Ticket.reporting.by_category(company='test_company0')
        >>> by_category_data
        {'headend': 3, 'receiver': 3}

        >>> by_category_data = Ticket.reporting.by_category()
        >>> by_category_data
        {
            'test_company0': {'headend': 3, 'receiver': 3},
            'test_company1': {'headend': 0, 'receiver': 3},
            'test_company2': {'headend': 0, 'receiver': 0}
        }

        :param company:
        :param year:
        :return:
        """
        if company:
            category_count = {category.name: 0 for category in Category.objects.all()}

            for ticket in self._get_qs(company, year):
                if ticket.category:
                    category_count[ticket.category.name] += 1

        else:
            category_count = {cmp.name: {c.name: 0 for c in Category.objects.all()} for cmp in Company.objects.all()}

            for company in category_count:
                for ticket in self._get_qs(company, year):
                    if ticket.category:
                        category_count[company][ticket.category.name] += 1

        return category_count

    def by_month(self, company='', year=datetime.date.today().year):
        """
        Ticket counts by month:

        >>> by_month_data = Ticket.reporting.by_month(company='test_company0')
        >>> by_month_data
        {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 6, 10: 0, 11: 0, 12: 0}
        >>> by_month_data = Ticket.reporting.by_month()
        >>> by_month_data
        {
            'test_company2': {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0},
            'test_company0': {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 6, 10: 0, 11: 0, 12: 0},
            'test_company1': {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 3, 10: 0, 11: 0, 12: 0}
        }
        :param company:
        :param year:
        :return:
        """
        if company:
            month_count = self._by_month_base()
            for ticket in super(TicketReportingManager, self)\
                    .filter(company__name=company)\
                    .filter(created__year=year):
                month_count[ticket.created.month] += 1
        else:
            month_count = self._by_month_base(all_companies=True)
            for company in month_count:
                for ticket in self._get_qs(company, year):
                    month_count[company][ticket.created.month] += 1

        return month_count

    def by_type(self, company='', year=datetime.date.today().year):
        """
        Ticket counts by type:

        >>> by_type_data = Ticket.reporting.by_type(company='test_company0')
        >>> by_type_data
        {
            'development': 3,
            'information': 0,
            'licensing': 0,
            'upgrade': 0,
            'install/setup': 0,
            'rma': 0,
            'question': 0,
            'issue': 3
        }
        >>> by_type_data = Ticket.reporting.by_type()
        >>> by_type_data
        {
            'test_company0': {
                'question': 0, 'information': 0, 'upgrade': 0, 'issue': 3,
                'install/setup': 0, 'rma': 0, 'licensing': 0, 'development': 3
            },
            'test_company2': {
                'question': 0, 'information': 0, 'upgrade': 0, 'issue': 0,
                'install/setup': 0, 'rma': 0, 'licensing': 0, 'development': 0
            },
            'test_company1': {
                'question': 0, 'information': 0, 'upgrade': 0, 'issue': 3,
                'install/setup': 0, 'rma': 0, 'licensing': 0, 'development': 0
            }
        }
        :param company:
        :param year:
        :return:
        """
        if company:
            type_count = {t[0]: 0 for t in td.TYPES}
            for ticket in self._get_qs(company, year):
                type_count[ticket.type] += 1
        else:
            type_count = {company.name: {t[0]: 0 for t in td.TYPES} for company in Company.objects.all()}
            for company in type_count:
                for ticket in self._get_qs(company, year):
                    type_count[company][ticket.type] += 1

        return type_count

    def _by_month_base(self, all_companies=False):
        if all_companies:
            return {company.name: self._by_month_base() for company in Company.objects.all()}
        else:
            return {month: 0 for month in range(1, 13)}

    def duration_by_month(self, company='', year=datetime.date.today().year):
        """
        Ticket counts by duration by month:

        >>> by_duration_data = Ticket.reporting.duration_by_month(company='test_company0')
        >>> by_duration_data
        {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 120, 10: 0, 11: 0, 12: 0}

        :param company:
        :param year:
        :return:
        """
        if company:
            duration_count = self._by_month_base()
            for ticket in self._get_qs(company, year):
                duration_count[ticket.created.month] += ticket.duration

        else:
            duration_count = self._by_month_base(all_companies=True)
            for company in duration_count:
                for ticket in self._get_qs(company, year):
                    duration_count[company][ticket.created.month] += ticket.duration

        return duration_count

    def _get_qs(self, company, year):
        return super(TicketReportingManager, self).filter(company__name=company).filter(created__year=year)


class Ticket(DirtyFieldsGrabberMixin, models.Model):
    assigned_to = models.ForeignKey(User, related_name='assigned_to', blank=True, null=True)
    category = models.ForeignKey(Category, related_name='category', null=True)
    company = models.ForeignKey(Company, related_name='company', blank=True, null=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    description = models.TextField()
    duration = models.IntegerField(default=0, blank=True, null=True)
    priority = models.CharField(max_length=30, choices=td.PRIORITIES, default=td.MED_PRIORITY)
    reported_by = models.ForeignKey(User, related_name='reported_by', null=True)
    site_name = models.CharField(max_length=250, blank=True)
    status = models.CharField(max_length=100, choices=td.STATUSES, default=td.OPEN, blank=True)
    tags = models.ManyToManyField(Tag, related_name='tags',  blank=True)
    title = models.CharField(max_length=250)
    type = models.CharField(max_length=100, choices=td.TYPES, default=td.ISSUE_TYPE)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    updated_by = models.ForeignKey(User, related_name='updated_by', null=True)

    logger = logging.getLogger('ticket')

    pre_link_msg = 'To view and reply to this ticket please visit the following link:'

    # keep default active
    objects = models.Manager()

    # Reporting  manager
    reporting = TicketReportingManager()

    # TODO check duration > 0 before allow close

    class Meta:
        ordering = ['-updated']

    def __str__(self):
        return '{0}: {1}'.format(self.id, self.title)

    @staticmethod
    def get_admin_subscribers():
        """Return email addressess for all admin subscribers in the system"""
        return [sub.owner.email for sub in AdminSubscriber.objects.all()]

    def get_email_subscribers(self):
        """Return email addressess for all email subscribers of ticket"""
        return [sub.address for sub in self.email_subscribers.all()]

    def get_user_subscribers(self):
        """Return email addressess for all subscribers of ticket"""
        return [sub.owner.email for sub in self.subscribers.all()]

    def get_twodb_subscribers(self):
        """Return email addressess for 2DB subscribers of ticket only"""
        return [sub.owner.email for sub in self.subscribers.filter(owner__profile__company='2DB')]

    def send_email_to_subscribers(self, subject: str, body: str, addresses: List[str]) -> None:
        """Send email to all active subscribers"""

        email = EmailMessage(
            subject='{0} - {1}'.format(self.get_email_subject_base(), subject),
            body='{0} \n{1}'.format(self.get_email_body_base(), body),
            to=addresses,
        )
        email.send()

    def get_absolute_url(self) -> str:
        return reverse('ticket:update_ticket', kwargs={'id': self.id})

    def get_desc_excerpt(self, chars: int) -> str:
        return self.description[:chars] + '...'

    def get_email_body_base(self) -> str:
        domain = Site.objects.get_current().domain
        return '''{subject} \n\n{pre_link_msg} \n\nhttp://{domain}{path}
        '''.format(
            subject=self.get_email_subject_base(),
            pre_link_msg=self.pre_link_msg,
            domain=domain,
            path=self.get_absolute_url()
        )

    def get_email_subject_base(self) -> str:
        return 'Ticket id: {0} {1}'.format(self.id, self.title)

    def has_subscriber(self, subscriber_id: int) -> int:
        subs = [sub for sub in self.subscribers.all() if sub.owner.id == subscriber_id]
        return len(subs)

    def merge_into(self, other_ticket) -> None:
        """Merge own ticket comments into other_ticket"""
        for comment in self.comments.all():
            comment.ticket = other_ticket
            comment.save()

        Event.objects.create(
            info='Ticket id: {}'.format(self.id),
            owner=self.updated_by,
            ticket=other_ticket,
            type='merge',
        )

    def save(self, *args, **kwargs) -> None:

        assigned_to_changed = self.check_if_field_dirty('assigned_to')

        if assigned_to_changed['changed']:

            owners = [s.owner for s in self.subscribers.all()]
            if not (self.assigned_to in owners):
                Subscriber.objects.create(
                    owner=self.assigned_to,
                    ticket=self
                )

            addresses = self.get_user_subscribers() + self.get_email_subscribers()

            self.send_email_to_subscribers(
                subject='Now Assigned To {user}'.format(user=self.assigned_to.username),
                body='This ticket is now assigned to {user}'.format(user=self.assigned_to.username),
                addresses=addresses
            )

        # TODO check if status has been changed

        super(Ticket, self).save(*args, **kwargs)


class Comment(models.Model):
    char_preview_limit = 20

    body = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    from_email = models.BooleanField(default=False)
    internal = models.BooleanField(default=False)
    owner = models.ForeignKey(User, related_name='owner', null=True)
    ticket = models.ForeignKey(Ticket, related_name='comments')
    sender = models.CharField(max_length=250, null=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return 'id:{id} - ticket:{ticket} - {body}'.format(
            id=self.id,
            ticket=self.ticket.id,
            body='{}...'.format(self.body[:self.char_preview_limit])
        )

    def get_owner_fullname(self):
        if self.owner:
            return '{owner.first_name} {owner.last_name}'.format(owner=self.owner)
        else:
            return ''

    def is_internal(self) -> bool:
        return self.internal

    def save(self, notify=True, *args, **kwargs) -> None:
        """Add Send Email when comment saved"""

        # only send emails if notify is true
        if notify:
            if self.internal:
                addresses = self.ticket.get_twodb_subscribers()
            else:
                addresses = self.ticket.get_user_subscribers() + self.ticket.get_email_subscribers()

            # remove sender
            try:
                addresses.remove(self.sender)
            except ValueError:
                pass

            self.ticket.send_email_to_subscribers(
                subject='Comment Added',
                body='Comment:\n\n{body} \n\n- {owner_fullname}'.format(
                    body=self.body,
                    owner_fullname=self.get_owner_fullname()
                ),
                addresses=addresses
            )

        self.ticket.updated_by = self.owner
        # trigger ticket.updated datetime to be updated
        self.ticket.save()

        super(Comment, self).save(*args, **kwargs)


class Event(models.Model):
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    info = models.TextField(null=True)
    owner = models.ForeignKey(User, related_name='event_owner', null=True)
    ticket = models.ForeignKey(Ticket, related_name='events')
    type = models.CharField(max_length=100, choices=td.EVENT_TYPES, default=td.CREATED_EVENT)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return '{0} - {1}'.format(self.type, self.info)


class EmailSubscriber(models.Model):
    address = models.EmailField(null=True)
    ticket = models.ForeignKey(Ticket, related_name='email_subscribers', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.address)


class FileAttachment(models.Model):
    raw_file = models.FileField(upload_to=settings.ATTACHMENT_UPLOAD_TO)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    comment = models.ForeignKey(Comment, related_name='file_attachments')

    def file_name(self):
        return os.path.basename(self.raw_file.name)

    def get_absolute_url(self):
        return '{}'.format(self.raw_file.url)

    def __str__(self):
        return '{}'.format(self.raw_file)


class Subscriber(models.Model):
    owner = models.ForeignKey(User, related_name='subscriber_owner', on_delete=models.CASCADE)
    ticket = models.ForeignKey(Ticket, related_name='subscribers', on_delete=models.CASCADE)

    def save(self, *args, **kwargs) -> None:
        # check for duplicate subscribers on tickets
        ticket = Ticket.objects.get(id=self.ticket.id)
        if ticket.has_subscriber(self.owner.id):
            raise ValueError('{0} is already subscribed to ticket {1}'.format(self.owner.username, self.ticket.id))
        else:
            super(Subscriber, self).save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.owner)


# signals ------------------------------------------------------------------

@receiver(post_save, sender=Ticket)
def add_ticket_event(sender, instance, created, update_fields, **kwargs):
    """Adds event when a ticket is created or updated"""
    # created event
    if created:
        Event.objects.create(
            info='',
            owner=instance.updated_by,
            ticket=instance,
            type='created',
        )
        # email admin subscribers advising a ticket has been created
        instance.send_email_to_subscribers(
            subject='New Ticket Created',
            body='Description: \n{}'.format(instance.description),
            addresses=Ticket.get_admin_subscribers()
        )

    # updated event
    else:
        info_string = ''

        changed_fields = instance.get_changed_fields_detail()

        if changed_fields:
            info_string = ' | '.join(changed_fields)

        Event.objects.create(
            info=info_string,
            owner=instance.updated_by,
            ticket=instance,
            type='updated',
        )


@receiver(post_save, sender=Comment)
def add_comment_event(sender, instance, created, update_fields, **kwargs):
    """Adds event when user creates a comment"""
    # if comment was generated via email, add the sender address to info
    if instance.sender:
        info = 'Sender: {}'.format(instance.sender)
    else:
        info = ''

    Event.objects.create(
        info=info,
        owner=instance.owner,
        ticket=instance.ticket,
        type='comment',
    )


@receiver(post_save, sender=Ticket)
def add_default_subscriber_to_ticket(sender, instance, created, update_fields, **kwargs):
    try:
        if created:
            Subscriber.objects.create(
                owner=instance.reported_by,
                ticket=instance
            )

        inst = instance

        # try to add assigned_to to subscribers
        if inst.reported_by != inst.assigned_to and inst.assigned_to not in [s.owner for s in inst.subscribers.all()]:
            Subscriber.objects.create(
                owner=instance.assigned_to,
                ticket=instance
            )
    except ObjectDoesNotExist as err:
        # TODO re raise as err
        # Subscriber has no owner (when generated by email)
        pass


# signals for on email events -----------------------------------------------
# @receiver(post_save, sender=Message)
# def create_ticket_from_email(sender, instance, **args):
#     print('in create_ticket_from_email')
#     Ticket.objects.create(
#         title=instance.subject,
#         description=instance.text
#     )


@receiver(message_received)
def create_ticket_from_email(sender, message, **args):
    from ticket.supportEmailHandler import EmailHandler
    logger = logging.getLogger('ticket')

    handler = EmailHandler(
        attachments=(message.attachments.all()),
        body=message.text,
        cc_list=message.get_email_object()['Cc'],
        msg_id=message.message_id,
        sender=message.from_header,
        subject=message.subject
    )
    try:
        handler.process()
    # Catch any exception to enable other emails to be processed and prevent a backlog
    except Exception as e:
        logging.error("Unable to process email - {subject} ({e})".format(subject=message.subject, e=e))

