from django.test import TestCase
from django.core.urlresolvers import reverse, resolve
from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.views import (
    EmailSubscriberCreateView,
    SubscriberListView
)
from ticket.models import EmailSubscriber, Ticket


class TestEmailSubscriberCreateView(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_data = {
        'address': 'test@test.com',
        'ticket': 1
    }
    invalid_data = {
        'address': 123,
        'ticket': 99
    }

    def setUp(self):
        self.url = reverse('ticket:create_email_subscriber')
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_email_subscriber_create_view(self):
        resolver = resolve(self.url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, EmailSubscriberCreateView),
            'url does not resolve to correct function'
        )

    def test_can_create_email_subscriber_via_post(self):
        response = self.client.post(self.url, data=self.valid_data, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.email_subscribers.count()
        self.assertEqual(sub_count, 1, 'Ticket does not have correct number of EmailSubscribers')
        self.assertEqual(response.status_code, 200, 'Unable to create email subscriber')

    def test_wont_create_email_subscriber_via_post_with_invalid_data(self):
        response = self.client.post(self.url, data=self.invalid_data, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.email_subscribers.count()
        self.assertEqual(sub_count, 0, 'Ticket does not have correct number of EmailSubscribers')


class TestEmailSubscriberDeleteView(TestCase):
    """Allows EmailSubscriber deletion via GET requests"""
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)
        ticket = Ticket.objects.get(id=1)
        EmailSubscriber.objects.create(address='test@test.com', ticket=ticket)

    def test_can_remove_subscriber_via_delete(self):
        url = reverse('ticket:delete_email_subscriber', kwargs={'id': 1})
        response = self.client.delete(url, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.email_subscribers.count()
        self.assertEqual(sub_count, 0, 'Ticket does not have correct number of EmailSubscribers')
        self.assertEqual(response.status_code, 200, 'Unable to add EmailSubscriber')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Email subscriber removed' in message.message)
