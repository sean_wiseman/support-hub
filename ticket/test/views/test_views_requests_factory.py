from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from ticket.views import TicketListView


# class TestTicketViews:
#     # def test_can_view_ticket_list_view(self):
#     #     req = RequestFactory().get(reverse('ticket:ticket_list'))
#     #     resp = TicketListView.as_view()(req)
#     #     assert resp.status_code == 200, 'Should be callable by anyone'


# class TestTicketAdminView:
#     def test_anon_user_cannot_see_view(self):
#         req = RequestFactory().get(reverse('ticket:ticket_list'))
#         req.user = AnonymousUser()
#
#         resp = TicketListView.as_view()(req)
#         assert 'login' in resp.url, 'reqest used: {}'.format(req)