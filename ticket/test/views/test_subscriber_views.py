from django.test import TestCase
from django.core.urlresolvers import reverse, resolve
from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.views import (
    SubscriberCreateView,
    SubscriberListView
)
from ticket.models import Subscriber, Ticket


class TestSubscriberCreateView(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_data = {
        'owner': 3,
        'ticket': 1
    }

    def setUp(self):
        self.url = reverse('ticket:create_subscriber')
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_subscriber_create_view(self):
        resolver = resolve(self.url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, SubscriberCreateView),
            'url does not resolve to correct function'
        )

    def test_can_create_subscriber_via_post(self):
        response = self.client.post(self.url, data=self.valid_data, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.subscribers.count()
        self.assertEqual(sub_count, 3, 'Ticket does not have correct number of Subscribers')
        self.assertEqual(response.status_code, 200, 'Unable to create subscriber')

    def test_will_handle_duplicate_subscriber_error(self):
        data = {
            'owner': 1,
            'ticket': 1
        }
        response = self.client.post(self.url, data=data, follow=True)
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue(
            'testUser1 is already subscribed to ticket 1' in message.message,
            'Error not present in response'
        )


class TestSubscriberAddView(TestCase):
    """Allows Subscriber additions via GET requests"""
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_add_subscriber_via_get(self):
        url = reverse('ticket:add_subscriber', kwargs={'user_id': 3, 'ticket_id': 1})
        response = self.client.get(url, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.subscribers.count()
        self.assertEqual(sub_count, 3, 'Ticket does not have correct number of Subscribers')
        self.assertEqual(response.status_code, 200, 'Unable to add subscriber')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Subscriber Added' in message.message)

    def test_will_fail_to_add_subscriber_with_invalid_data(self):
        url = reverse('ticket:add_subscriber', kwargs={'user_id': 0, 'ticket_id': 1})
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200, 'Unable to add subscriber')
        # check returned message
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('User Id or Ticket Id not valid' in message.message)


class TestSubscriberDeleteView(TestCase):
    """Allows Subscriber deletion via GET requests"""
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        user = create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

        ticket = Ticket.objects.get(id=1)

        Subscriber.objects.create(owner=user, ticket=ticket)

    def test_can_remove_subscriber_via_delete(self):
        url = reverse('ticket:delete_subscriber', kwargs={'user_id': 4, 'ticket_id': 1})
        response = self.client.get(url, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.subscribers.count()
        self.assertEqual(sub_count, 2, 'Ticket does not have correct number of Subscribers')
        self.assertEqual(response.status_code, 200, 'Unable to add subscriber')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Subscriber Removed' in message.message)

    def test_can_remove_subscriber_via_delete_with_user_redirect_url(self):
        ticket_id = 1
        params = {
            'user_id': 4,
            'ticket_id': ticket_id
        }
        custom_redirect_url = '?redirect_url={}'.format(reverse('ticket:subscriber_list', kwargs={'id': ticket_id}))

        url = reverse('ticket:delete_subscriber', kwargs=params)
        url += custom_redirect_url
        response = self.client.get(url, follow=True)
        ticket = Ticket.objects.get(id=1)
        sub_count = ticket.subscribers.count()

        self.assertEqual(sub_count, 2, 'Ticket does not have correct number of Subscribers')
        self.assertEqual(response.status_code, 200, 'Unable to add subscriber')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Subscriber Removed' in message.message)

    def test_wont_remove_subscriber_with_invalid_data(self):
        url = reverse('ticket:delete_subscriber', kwargs={'user_id': 99, 'ticket_id': 99})
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200, 'Unable to add subscriber')

        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('User Id or Ticket Id not valid' in message.message)

# TODO subscriber list view + create form
'''
    def test_context_includes_comment_form_in_addition_to_ticket_form(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200, 'Unable to request update view')
        self.assertTrue(response.context['form'], 'Ticket form not present in view')
        self.assertTrue(response.context['comment_form'], 'Comment form not present in view')
'''


class TestSubscribersListView(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_subscriber_list_view(self):
        resolver = resolve(reverse('ticket:subscriber_list', kwargs={'id': 1}))

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, SubscriberListView),
            'url does not resolve to correct function'
        )

    def test_can_return_view(self):
        url = reverse('ticket:subscriber_list', kwargs={'id': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch subscriber list view')

    def test_form_is_included_in_context(self):
        url = reverse('ticket:subscriber_list', kwargs={'id': 1})
        response = self.client.get(url)
        self.assertTrue(response.context['form'], 'Form not present in context')

    def test_subscriber_list_is_in_context(self):
        url = reverse('ticket:subscriber_list', kwargs={'id': 1})
        response = self.client.get(url)
        self.assertTrue(
            'subscriber_list' in response.context,
            'No Subscriber objects found in context'
        )
        self.assertEqual(
            response.context['subscriber_list'].count(),
            2,
            'Incorrect number of subscribers in context'
        )




