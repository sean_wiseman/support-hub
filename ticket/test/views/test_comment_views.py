from django.core.urlresolvers import reverse, resolve
from django.test import TestCase
from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.models import Comment, Ticket
from ticket.views import (
    CommentCreateView,
    CommentDeleteView,
)


class TestCommentCreateView(TestCase):
    fixtures = ['ticket_test_data.json']

    test_data = {
        'body': 'some test info',
        'owner': 1,
        'ticket': 2
    }

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_comment_create_view(self):
        resolver = resolve(reverse('ticket:create_comment'))

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, CommentCreateView),
            'url does not resolve to correct function'
        )

    def test_can_create_comment(self):
        url = reverse('ticket:create_comment')
        response = self.client.post(url, data=self.test_data)
        self.assertRedirects(response, '/2/update')

        self.assertEqual(response.status_code, 302, 'Unable to create comment')
        self.assertEqual(Comment.objects.count(), 6, 'Unable to create comment')

        # check that comment has been saved to correct ticket
        ticket = Ticket.objects.get(id=2)
        self.assertEqual(ticket.comments.count(), 1, 'ticket not created or assigned to ticket')
        self.assertEqual(ticket.comments.first().body, self.test_data['body'])

    def test_can_create_internal_comment(self):
        url = reverse('ticket:create_comment')

        internal_post_data = {
            'body': 'some test info',
            'internal': True,
            'owner': 1,
            'ticket': 2
        }
        response = self.client.post(url, data=internal_post_data)
        self.assertRedirects(response, '/2/update')

        self.assertEqual(response.status_code, 302, 'Unable to create comment')
        self.assertEqual(Comment.objects.count(), 6, 'Unable to create comment')

        # check that comment has been saved to correct ticket
        ticket = Ticket.objects.get(id=2)
        comment = ticket.comments.first()
        self.assertEqual(ticket.comments.count(), 1, 'ticket not created or assigned to ticket')
        self.assertEqual(comment.body, self.test_data['body'], 'Comment body not correct')
        self.assertTrue(comment.is_internal(), 'Comment is not flagged as internal')


class TestCommentDeleteView(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_comment_delete_view(self):
        resolver = resolve(reverse('ticket:delete_comment', kwargs={'id': 1}))

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, CommentDeleteView),
            'url does not resolve to correct function'
        )

    def test_can_delete_comment(self):
        self.assertEqual(Comment.objects.count(), 5, 'Number of comment fixtures not correct')

        url = reverse('ticket:delete_comment', kwargs={'id': 1})
        response = self.client.post(url)
        self.assertEqual(response.status_code, 302, 'Unable to delete comment')
        self.assertRedirects(response, '/1/update')
        self.assertEqual(Comment.objects.count(), 4, 'Comment not deleted')
