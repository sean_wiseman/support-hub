from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, resolve
from django.test import RequestFactory, TestCase

from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.models import (
    Tag,
    Ticket
)
from ticket.views import (
    TicketListView,
    TicketMergeView,
    ticket_multi_delete_view,
    ticket_multi_merge_view,
    TicketUpdateView
)


class TestTicketViews(TestCase):
    fixtures = ['ticket_test_data.json']

    @classmethod
    def setUpClass(cls):
        super(TestTicketViews, cls).setUpClass()
        cls.ticket_update_url = reverse('ticket:update_ticket', kwargs={'id': 1})
        cls.ticket_list_url = reverse('ticket:ticket_list')

    def setUp(self):
        self.user = create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_ticket_detail_view(self):
        resolver = resolve(self.ticket_update_url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, TicketUpdateView),
            'url does not resolve to correct function'
        )

    def test_can_resolve_ticket_list_view(self):
        resolver = resolve(self.ticket_list_url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, TicketListView),
            'url does not resolve to correct function'
        )

    def test_can_view_ticket_details_view(self):
        response = self.client.get(self.ticket_update_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket detail')

    def test_can_view_ticket_list_view(self):
        response = self.client.get(self.ticket_list_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')

    def test_list_view_has_ticket_objects_in_context(self):
        response = self.client.get(self.ticket_list_url)

        self.assertTrue(
            'ticket_list' in response.context,
            'No ticket objects found in context'
        )
        self.assertEqual(len(response.context['ticket_list']), 3, 'No tickets in context')

    def test_detail_view_has_ticket_object_in_context(self):
        response = self.client.get(self.ticket_update_url)

        self.assertTrue('ticket' in response.context, 'No ticket object found in context')

        ticket = response.context['ticket']
        self.assertEqual(ticket.comments.count(), 2, '')

    def test_can_set_order_by_value(self):
        order_by = 'id'

        session = self.client.session
        session['order_by'] = order_by
        response = self.client.get(self.ticket_list_url + '?order_by=' + order_by)

        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')

        first_ticket = response.context['ticket_list'].first()
        self.assertEqual(first_ticket.id, 1, 'Tickets are not in id order')

        # make another request with 'id' which should now order by reverse id
        response = self.client.get(self.ticket_list_url + '?order_by=' + order_by)

        first_ticket = response.context['ticket_list'].first()
        self.assertEqual(first_ticket.id, 3, 'Tickets are not in reverse id order')

        # make sure order_by will reverse -id back to id if user requests id again
        order_by = '-id'

        session = self.client.session
        session['order_by'] = order_by
        response = self.client.get(self.ticket_list_url + '?order_by=' + order_by)

        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')

    def test_as_twodb_user_can_see_all_tickets(self):
        response = self.client.get(self.ticket_list_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')
        self.assertTrue(response.context['ticket_list'], 'Tickets not present in context')
        self.assertEqual(response.context['ticket_list'].count(), 3, 'Number of Tickets in context is not correct')

    def test_can_only_view_tickets_that_match_user_profile_company(self):
        self.user.profile.company = 'some_company'
        self.user.profile.save()

        response = self.client.get(self.ticket_list_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')
        self.assertTrue(response.context['ticket_list'], 'Tickets are present in context')
        self.assertEqual(response.context['ticket_list'].count(), 1, 'Number of Tickets in context is not correct')

    def test_will_not_get_any_tickets_if_blank_user_profile_company(self):
        self.user.profile.company = 'some_other_company'
        self.user.profile.save()

        response = self.client.get(self.ticket_list_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket list')
        self.assertFalse(response.context['ticket_list'], 'Tickets are present in context')
        self.assertEqual(len(response.context['ticket_list']), 0, 'Number of Tickets in context is not correct')


class TestTicketCreateView(TestCase):
    fixtures = ['ticket_test_data.json']
    test_data = {
        'category': 1,
        'company': '2DB',
        'description': 'Some temp desc',
        'duration': 0,
        'priority': 'high',
        'status': 'Open',
        'assigned_to': 1,
        'site_name': 'test site',
        'title': 'test ticket create',
        'type': 'issue'
    }
    test_un = 'testUserTemp'
    test_pw = 'somepassword1'

    def test_can_create_ticket(self):
        user = create_user(un=self.test_un, pw=self.test_pw)
        self.client.login(username=self.test_un, password=self.test_pw)

        url = reverse('ticket:create_ticket')
        response = self.client.post(url, data=self.test_data)
        self.assertRedirects(response, '/')

        self.assertEqual(response.status_code, 302, 'Unable to create ticket')
        self.assertEqual(Ticket.objects.count(), 4, 'Unable to create ticket')

        # make sure ticket.reported_by is populated
        new_ticket = Ticket.objects.get(title='test ticket create')
        self.assertEqual(new_ticket.reported_by, user, 'reported by user not correct')

        # check success message was provided
        # self.assertEqual(response.context['messages'], 1)


class TestTicketDeleteView(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_delete_ticket(self):
        self.assertEqual(Ticket.objects.count(), 3, 'Number of Ticket fixtures not correct')

        url = reverse('ticket:delete_ticket', kwargs={'id': 1})
        response = self.client.delete(url)
        self.assertRedirects(response, '/')

        self.assertEqual(response.status_code, 302, 'Unable to delete ticket')
        self.assertEqual(Ticket.objects.count(), 2, 'Ticket was not deleted from db')


class TestTicketUpdateView(TestCase):
    fixtures = ['ticket_test_data.json']
    test_un = 'testUserTemp'
    test_pw = 'somepassword1'
    test_data = {
        'category': 1,
        'company': '2DB',
        'description': 'Some temp desc',
        'duration': 0,
        'priority': 'high',
        'reported_by': 1,
        'status': 'Open',
        'assigned_to': 1,
        'site_name': 'test site',
        'title': 'test ticket',
        'type': 'issue'
    }

    def setUp(self):
        self.url = reverse('ticket:update_ticket', kwargs={'id': 1})
        self.user = create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_update_ticket(self):
        self.assertEqual(Ticket.objects.count(), 3, 'Number of Ticket fixtures not correct')
        response = self.client.post(self.url, data=self.test_data)
        self.assertEqual(response.status_code, 200, 'Unable to Update ticket')

    def test_context_includes_sub_forms_in_addition_to_ticket_form(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200, 'Unable to request update view')
        self.assertTrue(response.context['form'], 'Ticket form not present in view')
        self.assertTrue(response.context['comment_form'], 'Comment form not present in view')
        self.assertTrue(response.context['tag_create_form'], 'TagCreateForm form not present in view')

    def test_updated_by_user_is_processed_by_form_valid_method(self):
        # get target ticket object and check updated_by user
        ticket = Ticket.objects.get(id=1)
        self.assertEqual(ticket.updated_by_id, 2, 'Updated by user not as expected')

        # create user and login
        self.assertEqual(
            self.client.session.get('_auth_user_id'),
            '4',
            'New User id not as expected'
        )

        response = self.client.post(self.url, data=self.test_data)
        self.assertEqual(response.status_code, 200, 'Unable to Update ticket')
        # check that user 1 is now the updated_by user
        ticket = Ticket.objects.get(id=1)
        self.assertEqual(ticket.updated_by_id, 2, 'Updated by user not as expected')

    def test_cannot_view_ticket_if_company_does_not_match_user_profile(self):
        # switch user to non 2DB
        self.user.profile.company = 'some_company'
        self.user.profile.save()
        # try to view ticket which does not match user profile company
        response = self.client.get(self.url, follow=True)
        self.assertEqual(response.status_code, 200, 'Was not redirected to ticket list when expected')

        # check error is in response
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('You do not have permission to view ticket 1' in message.message)


class TestTicketQuickSearchView(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_redirect_to_valid_ticket_id(self):
        response = self.client.get(reverse('ticket:ticket_quick_search'), {'ticket_id': 1})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/1/update')

    def test_will_redirect_to_ticket_list_if_ticket_does_not_exist(self):
        response = self.client.get(
            reverse('ticket:ticket_quick_search'),
            {'ticket_id': 99},
            follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, '/')

        # check returned message
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('No Ticket found' in message.message)


class TestTicketMergeView(TestCase):
    fixtures = ['ticket_test_data.json']

    @classmethod
    def setUpClass(cls):
        super(TestTicketMergeView, cls).setUpClass()
        cls.ticket_merge_url = reverse('ticket:merge_ticket', kwargs={'id': 1})

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_ticket_merge_view(self):
        resolver = resolve(self.ticket_merge_url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, TicketMergeView),
            'url does not resolve to correct function'
        )

    def test_can_return_form_view(self):
        response = self.client.get(self.ticket_merge_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket merge view')
        # self.assertEqual(response.context['resolver_match'], )

    def test_can_merge_tickets_via_view(self):
        ticket_1 = Ticket.objects.get(id=1)
        ticket_2 = Ticket.objects.get(id=2)
        # check current comment counts
        self.assertEqual(ticket_1.comments.count(), 2)
        self.assertEqual(ticket_2.comments.count(), 0)

        post_data = {
            'from_ticket': ticket_1.id,
            'to_ticket': ticket_2.id
        }

        response = self.client.post(self.ticket_merge_url, data=post_data)
        self.assertEqual(response.status_code, 302, 'Unable to fetch ticket merge view')
        self.assertRedirects(response, '/2/update')

        # check that ticket 1 comments have been merged into ticket 2
        self.assertEqual(ticket_1.comments.count(), 0)
        self.assertEqual(ticket_2.comments.count(), 2)


class TestTicketMultiSelectView(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_test_data = {'tickets_selected': ['1', '2', '3']}

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_receive_multiple_ticket_ids_in_post_view(self):
        response = self.client.post(
            reverse('ticket:multi_select'),
            data=self.valid_test_data
        )
        self.assertTrue(response.status_code, 200)
        self.assertEqual(
            response.context['selected_tickets'],
            self.valid_test_data['tickets_selected'],
            'Selected tickets not correct in response context'
        )
        self.assertEqual(
            self.client.session.get('selected_tickets', None),
            response.context['selected_tickets'],
            'Selected tickets not present and correct in session'
        )

    def test_will_redirect_with_error_msg_if_no_tickets_selected(self):
        response = self.client.post(
            reverse('ticket:multi_select'),
            data={},
            follow=True
        )
        self.assertRedirects(
            response,
            '/',
            status_code=302,
            target_status_code=200
        )
        # check returned message
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('No tickets were selected' in message.message)

    def test_will_redirect_if_called_with_get(self):
        response = self.client.get(reverse('ticket:multi_select'))
        self.assertRedirects(
            response,
            '/',
            status_code=302,
            target_status_code=200
        )


class TestTicketMultiMergeView(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_test_data = {'to_ticket': 2}

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_merge_selected_tickets_into_target_ticket(self):
        # check ticket 2 comments len
        ticket = Ticket.objects.get(id=2)
        self.assertEqual(len(ticket.comments.all()), 0, 'Ticket comments total not as expected')

        # generate request manually to allow addition of tickets_selected
        # note: cannot add this to session and pass in whilst testing
        request = RequestFactory().post(
            reverse('ticket:multi_merge'),
            data=self.valid_test_data
        )

        # add authenticated user
        request.user = User.objects.get(username=test_un)

        response = ticket_multi_merge_view(request, selected_tickets=['1', '3'])
        self.assertTrue(response.status_code, 200)

        # check ticket 2's comments len, should have all comments from merged tickets
        self.assertEqual(len(ticket.comments.all()), 5, 'Ticket comments have not been merged')

    def test_can_handle_selected_tickets_not_being_present_in_session(self):
        response = self.client.post(
            reverse('ticket:multi_merge'),
            data=self.valid_test_data,
            follow=True
        )
        message = list(response.context.get('messages'))[0]
        self.assertTrue(response.status_code, 200)
        self.assertRedirects(response, '/')
        self.assertEqual(message.tags, 'error')
        self.assertTrue('No tickets were selected' in message.message)


class TestTicketMultiDeleteView(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_delete_selected_tickets(self):
        self.assertEqual(Ticket.objects.all().count(), 3)
        # generate request manually to allow addition of tickets_selected
        # note: cannot add this to session and pass in whilst testing
        request = RequestFactory().get(reverse('ticket:multi_delete'))

        # add authenticated user
        request.user = User.objects.get(username=test_un)

        response = ticket_multi_delete_view(request, selected_tickets=['1', '3'])
        self.assertEqual(Ticket.objects.all().count(), 1, 'Tickets not deleted')
        self.assertTrue(response.status_code, 200)

    def test_can_handle_selected_tickets_not_being_present_in_session(self):
        response = self.client.get(
            reverse('ticket:multi_delete'),
            follow=True
        )

        message = list(response.context.get('messages'))[0]
        self.assertTrue(response.status_code, 200)
        self.assertRedirects(response, '/')
        self.assertEqual(message.tags, 'error')
        self.assertTrue('No tickets were selected' in message.message)


class TestTagCreateAndAddView(TestCase):
    fixtures = ['ticket_test_data.json']
    test_ticket_id = 1

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_create_and_add_single_tag(self):
        ticket = Ticket.objects.get(id=self.test_ticket_id)
        url = reverse('ticket:create_tag')
        post_data = {'name': 'test tag', 'ticket_id': self.test_ticket_id}
        response = self.client.post(url, data=post_data)
        self.assertRedirects(
            response,
            '/{}/update'.format(self.test_ticket_id),
            status_code=302
        )
        self.assertEqual(ticket.tags.count(), 4, 'Tag was not created')

    def test_can_create_and_add_multiple_tags(self):
        ticket = Ticket.objects.get(id=self.test_ticket_id)
        url = reverse('ticket:create_tag')
        post_data = {'name': 'test tag, test2', 'ticket_id': self.test_ticket_id}
        response = self.client.post(url, data=post_data, follow=True)
        self.assertRedirects(
            response,
            '/{}/update'.format(self.test_ticket_id),
            status_code=302
        )
        self.assertEqual(ticket.tags.count(), 5, 'Tag was not created')
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Tag(s) Added' in message.message)

    def test_will_fail_with_invalid_data(self):
        url = reverse('ticket:create_tag')
        post_data = {'name': '', 'ticket_id': ''}
        response = self.client.post(url, data=post_data, follow=True)
        self.assertRedirects(response, '/')
        # check error is in response
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('Tag(s) Not Added' in message.message)


class TestTagRemoveView(TestCase):
    fixtures = ['ticket_test_data.json']
    test_ticket_id = 1

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_remove_tag_from_ticket(self):
        ticket = Ticket.objects.get(id=self.test_ticket_id)
        url = reverse('ticket:remove_tag', kwargs={'tag_id': 1, 'ticket_id': 1})

        response = self.client.get(url, follow=True)
        self.assertRedirects(
            response,
            '/{}/update'.format(self.test_ticket_id),
            status_code=302
        )
        self.assertEqual(ticket.tags.count(), 2, 'Tag was not removed')
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'success')
        self.assertTrue('Tag Removed' in message.message)

    def test_can_handle_invalid_data(self):
        url = reverse('ticket:remove_tag', kwargs={'tag_id': 'test', 'ticket_id': 'test'})

        response = self.client.get(url, follow=True)
        self.assertRedirects(
            response,
            '/',
            status_code=302
        )
        message = list(response.context.get('messages'))[0]
        self.assertEqual(message.tags, 'error')
        self.assertTrue('Tag Id or Ticket Id not valid' in message.message)

# TODO test DirtyFieldsGrabberMixin

# TODO test ListOderByMixin

