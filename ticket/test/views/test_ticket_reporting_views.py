from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy, reverse, resolve
from django.test import TestCase

from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.views import TicketReportingView


class TestTicketReportingViews(TestCase):
    fixtures = ['ticket_test_data.json']
    url = reverse_lazy('ticket:ticket_reporting')
    valid_data = {
        'company': '2DB',
        'from_date': '09/06/2017',
        'to_date': '09/07/2017',
    }

    def setUp(self):
        create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)

    def test_can_resolve_ticket_reporting_view(self):
        resolver = resolve(self.url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, TicketReportingView),
            'url does not resolve to correct function'
        )

    def test_form_is_provided_in_reponse_context(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200, 'Unable to request reporting view')
        self.assertTrue(response.context['form'], 'Reporting form not present in response')

    # def test_report_context_is_present_in_post_response(self):
    #     response = self.client.post(self.url, data=self.valid_data)
    #     self.assertEqual(response.status_code, 200, 'Unable to request reporting view')
    #     self.assertTrue(response.context['report'], 'Report context not present in response')
