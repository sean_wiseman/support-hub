from django.test import TestCase
from django.core.urlresolvers import reverse, resolve, reverse_lazy
from testingUtils.utils import (
    create_user,
    resolves_to_correct_func,
    test_un,
    test_pw
)
from ticket.models import Ticket
from ticket.views import TicketSearchView


class TestTicketSearchView(TestCase):

    def setUp(self):
        self.user = create_user(test_un, test_pw)
        self.client.login(username=test_un, password=test_pw)
        self.ticket_search_url = reverse_lazy('ticket:ticket_search')

    def test_can_resolve_ticket_search_view(self):
        resolver = resolve(self.ticket_search_url)

        self.assertEqual(resolver.namespace, 'ticket', 'url namespace incorrect')
        self.assertTrue(
            resolves_to_correct_func(resolver, TicketSearchView),
            'url does not resolve to correct function'
        )

    def test_can_view_ticket_search_view(self):
        response = self.client.get(self.ticket_search_url)
        self.assertEqual(response.status_code, 200, 'Unable to fetch ticket searchc view')
