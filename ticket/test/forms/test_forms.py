from django.test import TestCase
from django.contrib.auth.models import User
from ticket.forms import (
    CommentCreateForm,
    EmailSubscriberForm,
    SubscriberForm,
    TagCreateAndAddForm,
    TagRemoveForm,
    TicketForm,
    TicketMergeForm,
    TicketMultiMergeForm,
    TicketReportingForm
)
from ticket.models import (
    Comment,
    EmailSubscriber,
    Subscriber,
    Tag,
    Ticket
)
import datetime


class TestTicketCreateForm(TestCase):
    fixtures = ['ticket_test_data.json']

    # values for related fields are primary keys from fixtures
    multi_test_data = {
        'category': 1,
        'company': '2DB',
        'description': 'Some temp desc',
        'duration': 0,
        'priority': 'high',
        'reported_by': 1,
        'status': 'Open',
        'assigned_to': 1,
        'site_name': 'test site',
        'title': 'test ticket',
        'type': 'issue'
    }

    test_invalid_data = {
        'category': '',
        'company': '',
        'description': '',
        'duration': 0,
        'priority': '',
        'site_name': '',
        'title': '',
        'type': ''
    }

    def test_can_create_ticket_with_form(self):
        user = User.objects.first()
        form = TicketForm(self.multi_test_data)
        self.assertTrue(form.is_valid(), msg='Ticket form is not valid')

        form.save(reported_by_user=user, updated_by_user=user)
        self.assertEqual(Ticket.objects.count(), 4, msg='Unable to create ticket')

    def test_will_fail_with_invalid_data(self):
        form = TicketForm(self.test_invalid_data)
        self.assertFalse(form.is_valid(), msg='Ticket form accepted invalid data')


class TestTicketMergeForm(TestCase):

    def test_can_validate(self):
        form = TicketMergeForm({'from_ticket': 1, 'to_ticket': 2})
        self.assertTrue(form.is_valid(), msg='Form data is not valid')

    def test_will_fail_validation(self):
        form = TicketMergeForm({'from_ticket': 'invalid', 'to_ticket': 'invalid'})
        self.assertFalse(form.is_valid(), msg='Form data has failed to detect invalid data')


class TestTicketMultiMergeForm(TestCase):

    def test_can_validate(self):
        form = TicketMultiMergeForm({'to_ticket': 2})
        self.assertTrue(form.is_valid(), msg='Form data is not valid')

    def test_will_fail_validation(self):
        form = TicketMultiMergeForm({'to_ticket': 'invalid'})
        self.assertFalse(form.is_valid(), msg='Form data has failed to detect invalid data')


class TestCommentCreateForm(TestCase):
    fixtures = ['ticket_test_data.json']
    multi_test_data = {
        'body': 'some test info',
        'owner': 1,
        'ticket': 1
    }

    def test_can_create_comment_with_form(self):
        form = CommentCreateForm(self.multi_test_data)
        self.assertTrue(form.is_valid(), msg='Comment form is not valid')

        form.save()

        self.assertEqual(Comment.objects.count(), 6, msg='Unable to create comment')

    def test_can_create_internal_comment_with_form(self):
        internal_post_data = {
            'body': 'some test info',
            'internal': True,
            'owner': 1,
            'ticket': 1
        }
        form = CommentCreateForm(internal_post_data)
        self.assertTrue(form.is_valid(), msg='Comment form is not valid')

        form.save()

        self.assertEqual(Comment.objects.count(), 6, msg='Unable to create comment')


class TestEmailSubscriberCreateForm(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_data = {
        'address': 'test@test.com',
        'ticket': 1
    }
    invalid_data = {
        'address': 99,
        'ticket': 99
    }

    def test_can_create_email_subscriber_with_form(self):
        form = EmailSubscriberForm(self.valid_data)

        self.assertTrue(form.is_valid(), 'Form data is not valid')
        form.save()
        self.assertEqual(EmailSubscriber.objects.count(), 1, 'EmailSubscriber has not been created')

    def test_will_reject_invalid_data(self):
        form = EmailSubscriberForm(self.invalid_data)
        self.assertFalse(form.is_valid(), 'Form data is wrongly seen as valid')


class TestSubscriberCreateForm(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_data = {
        'owner': 3,
        'ticket': 1
    }
    invalid_data = {
        'owner': 99,
        'ticket': 99
    }

    def test_can_create_subscriber_with_form(self):
        form = SubscriberForm(self.valid_data)

        self.assertTrue(form.is_valid(), 'Form data is not valid')
        form.save()
        self.assertEqual(Subscriber.objects.count(), 7, 'Subscriber has not been created')

    def test_will_reject_invalid_data(self):
        form = SubscriberForm(self.invalid_data)
        self.assertFalse(form.is_valid(), 'Form data is wrongly seen as valid')


class TestTagCreateAndAddForm(TestCase):
    fixtures = ['ticket_test_data.json']
    multi_test_data = 'test tag, another, last one'
    test_ticket_id = 1

    def test_can_parse_multi_name_data(self):
        result = TagCreateAndAddForm.parse_multi_name_data(self.multi_test_data)
        self.assertEqual(
            result,
            ['test tag', 'another', 'last one'],
            'List was not generated correctly'
        )

    def test_can_create_single_tag_with_form_and_add_to_ticket(self):
        ticket = Ticket.objects.get(id=self.test_ticket_id)
        form = TagCreateAndAddForm(
            {
                'name': 'test tag',
                'ticket_id': self.test_ticket_id
            }
        )
        self.assertTrue(form.is_valid(), 'Form data is not valid')
        form.save()
        self.assertEqual(Tag.objects.count(), 4, 'Tag was not created')
        self.assertEqual(ticket.tags.count(), 4, 'Tag was not added to ticket')

    def test_can_create_multiple_tags_with_form_and_add_to_ticket(self):
        ticket = Ticket.objects.get(id=self.test_ticket_id)
        form = TagCreateAndAddForm(
            {
                'name': self.multi_test_data,
                'ticket_id': self.test_ticket_id
            }
        )
        self.assertTrue(form.is_valid(), 'Form data is not valid')
        form.save()
        self.assertEqual(Tag.objects.count(), 6, 'Tags were not created')
        self.assertEqual(ticket.tags.count(), 6, 'Tag was not added to ticket')

    # TODO will error if invalid values e.g. ','
    # TODO test will cope with ticket_id not being sent
    # TODO test that cant add duplicates


class TestTagRemoveForm(TestCase):
    valid_data = {'tag_id': 1, 'ticket_id': 1}
    invalid_data = {'tag_id': 'test', 'ticket_id': 'test'}

    def test_handles_valid_data(self):
        form = TagRemoveForm(self.valid_data)
        self.assertTrue(form.is_valid())

    def test_handles_invalid_data(self):
        form = TagRemoveForm(self.invalid_data)
        self.assertFalse(form.is_valid(), 'Has not detected invalid data')


class TestTicketReportingForm(TestCase):
    fixtures = ['ticket_test_data.json']
    valid_data = {
        'company': '2DB',
        'year': 2017
    }

    invalid_data = {
        'company': 123,
        'year': '',
    }

    def test_handles_valid_data(self):
        form = TicketReportingForm(self.valid_data)
        self.assertTrue(form.is_valid(), 'form cannot process valid data')
        self.assertEqual(form.cleaned_data['company'].name, '2DB', 'Company does not match expected value')
        self.assertEqual(
            form.cleaned_data['year'],
            2017,
            'year does not match expected value'
        )
        self.assertEqual(
            form.cleaned_data['year'],
            2017,
            'year does not match expected value'
        )

    def test_handles_invalid_data(self):
        form = TicketReportingForm(self.invalid_data)
        self.assertFalse(form.is_valid(), 'form cannot handle invalid data')



