from django.test import TestCase
from django.core import mail
from django.core.files.uploadedfile import SimpleUploadedFile
from django_mailbox.models import MessageAttachment
from ticket.supportEmailHandler import EmailHandler
from ticket.models import Ticket
from unittest.mock import MagicMock


msg_fixture = {
    'attachments': [],
    'body': 'Some body text',
    'cc_list': 'Some Guy <someguy@test.com>, Some OtherGuy <someotherguy@test.com>',
    'msg_id': '<CAN9qYMWddFcfGSNHptCPSGai4mLNHO0_tOde1_AYYJTz7qD7+w@mail.gmail.com>',
    'sender': 'Sean Wiseman <swiseman@2db.com>',
    'subject': 'Re: Ticket id: 1 some other subject'
}

msg_fixture_new_ticket = {
    'attachments': [],
    'body': 'Some body text',
    'cc_list': 'Some Guy <someguy@test.com>, Some OtherGuy <someotherguy@test.com>',
    'msg_id': '<CAN9qYMWddFcfGSNHptCPSGai4mLNHO0_tOde1_AYYJTz7qD7+w@mail.gmail.com>',
    'sender': 'Sean Wiseman <swiseman@2db.com>',
    'subject': 'Some subject'
}


def create_attachment():
    mock_attch = MagicMock(spec=MessageAttachment)
    mock_attch.document = SimpleUploadedFile('some_test_upload.txt', b'file contents!')
    return mock_attch


class TestSupportEmailHandler(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.handler = EmailHandler(**msg_fixture)

    def test_can_init_handler(self):
        self.assertTrue(self.handler, 'Cannot init handler')

    def test_can_retrieve_attributes_after_init(self):
        self.assertEqual(self.handler.body, msg_fixture['body'], 'Handler attribute not valid')
        self.assertEqual(self.handler.has_ticket_ref, True, 'Handler attribute not valid')
        self.assertEqual(self.handler.msg_id, msg_fixture['msg_id'], 'Handler attribute not valid')
        self.assertEqual(self.handler.sender, 'swiseman@2db.com', 'Handler attribute not valid')
        self.assertEqual(self.handler.ticket_id, 1, 'Handler attribute not valid')
        self.assertEqual(
            self.handler.cc_list,
            ['someguy@test.com', 'someotherguy@test.com'],
            'Handler attribute not valid'
        )

    def test_can_find_ticket_id_in_subject(self):
        subjects = {
            1010: 'Ticket id: 1010',
            1010111: 'Re: Ticket id: 1010111 some other subject',
            34: 'Ticket id: 34 some other subject 1011',
            234567: 'Ticket id: 234567 some 1234',
            56767867: 'some long pre text Ticket id: 56767867 some ',
        }
        for ticket_id, subject in subjects.items():
            with self.subTest():
                self.assertEqual(self.handler.find_ticket_id(subject), ticket_id, 'Unable to find ticket id')

    def test_wont_find_ticket_id_in_subject(self):
        subjects = {
            1010: 'Ticket id1010',
            1010111: 'Re: Ticket 1010111 some other subject',
            34: 'id: 34 some other subject 1011',
            234567: 'some 1234',
            56767867: 'some long pre text Ticket: 56767867 some ',
        }
        for ticket_id, subject in subjects.items():
            with self.subTest():
                self.assertNotEqual(self.handler.find_ticket_id(subject), ticket_id, 'Should not find ticket id')

    def test_does_not_have_ticket_ref_when_subject_does_not_contain_ticket_id(self):
        handler = EmailHandler(**msg_fixture_new_ticket)
        self.assertFalse(handler.has_ticket_ref, 'Should not have found an existing ticket id')

    def test_can_get_cc_list(self):
        self.assertTrue(self.handler.cc_list, 'CC list not valid')

    def test_can_parse_valid_cc_list(self):
        cc_list = 'Some Guy <someguy@test.com>, Some OtherGuy <someotherguy@test.com>'
        self.assertEqual(
            self.handler.parse_cc_list(cc_list),
            ['someguy@test.com', 'someotherguy@test.com'],
            'Unable to parse cc list'
        )

    def test_will_return_empty_list_if_try_to_parse_empty_cc_list(self):
        cc_list = []
        self.assertEqual(
            self.handler.parse_cc_list(cc_list),
            [],
            'Unable to parse empty cc list'
        )

    def test_can_parse_str_for_email_address(self):
        sender = 'Sean Wiseman <swiseman@2db.com>'
        self.assertEqual(
            self.handler.find_address(sender),
            'swiseman@2db.com',
            'Unable to find email address in str'
        )

    def test_will_return_none_if_cant_find_email_address_in_sender_str(self):
        sender = 'Sean Wiseman qwerty1234'
        self.assertFalse(
            self.handler.find_address(sender),
            'Unable did not return none when failing to find email address in str'
        )

    def test_can_parse_str_for_multiple_email_addresses(self):
        cc_list = [
            'Some Guy <someguy@test.com>',
            'someotherguy@test.com',
            'Sean Wiseman <swiseman@2db.com>'
        ]
        parsed_cc_list = [self.handler.find_address(addr) for addr in cc_list]
        self.assertEqual(len(parsed_cc_list), 3, 'Incorrect number of parsed addresses')
        self.assertEqual(
            parsed_cc_list,
            ['someguy@test.com', 'someotherguy@test.com', 'swiseman@2db.com'],
            'Unable to find all email addresses for cc list'
        )

    def test_can_create_new_ticket(self):
        self.handler.create_ticket()
        self.assertEqual(Ticket.objects.count(), 4, 'Ticket was not created')

    def test_can_add_email_subscribers(self):
        ticket = Ticket.objects.get(id=1)
        self.handler.add_email_subscribers(ticket=ticket)
        self.assertEqual(ticket.email_subscribers.count(), 3, 'Email subscribers not added')

    def test_can_attach_comment_to_existing_ticket(self):
        ticket = Ticket.objects.get(id=1)
        self.handler.add_comment_to_existing_ticket(ticket, self.handler.get_email_body_for_comment())
        latest_comment = ticket.comments.first()
        self.assertEqual(latest_comment.sender, 'swiseman@2db.com', 'Comment sender not correct')
        self.assertEqual(
            latest_comment.body,
            'cc: {} \n\nSome body text'.format(self.handler.cc_list),
            'Comment body not correct'
        )

    def test_will_create_new_ticket_during_processing_doesnt_have_ticket_ref(self):
        handler = EmailHandler(**msg_fixture_new_ticket)
        handler.process()
        self.assertFalse(handler.has_ticket_ref, 'Handler should not have ticket ref')
        self.assertEqual(Ticket.objects.count(), 4, 'Ticket was not created')

    def test_will_create_new_ticket_during_processing_doesnt_have_ticket_ref_with_attachements(self):
        fixture = msg_fixture_new_ticket.copy()
        fixture['attachments'] = [create_attachment() for i in range(4)]

        handler = EmailHandler(**fixture)
        handler.process()
        self.assertFalse(handler.has_ticket_ref, 'Handler should not have ticket ref')
        self.assertEqual(Ticket.objects.count(), 4, 'Ticket was not created')

    def test_can_run_process_func_for_existing_ticket(self):
        ticket = Ticket.objects.get(id=1)
        self.handler.process()
        latest_comment = ticket.comments.first()
        self.assertEqual(latest_comment.sender, 'swiseman@2db.com', 'Comment sender not correct')
        self.assertEqual(
            latest_comment.body,
            'cc: {} \n\nSome body text'.format(self.handler.cc_list),
            'Comment body not correct'
        )

    def test_can_send_auto_reply_email(self):
        ticket = Ticket.objects.get(id=1)
        self.handler.send_auto_reply_email(ticket)
        self.assertEqual(len(mail.outbox), 1, 'Email not sent')

    def test_can_receive_and_add_attachment(self):
        mock_attch = create_attachment()

        ticket = Ticket.objects.get(id=1)
        fixture = msg_fixture.copy()
        fixture['attachments'] = [mock_attch]
        self.handler = EmailHandler(**fixture)
        self.handler.process()
        latest_comment = ticket.comments.first()
        self.assertEqual(latest_comment.file_attachments.count(), 1, 'Comment does not have attachment')

    def test_can_receive_and_add_multiple_attachment(self):
        ticket = Ticket.objects.get(id=1)
        fixture = msg_fixture.copy()
        fixture['attachments'] = [create_attachment() for i in range(4)]

        self.handler = EmailHandler(**fixture)
        self.handler.process()
        latest_comment = ticket.comments.first()
        self.assertEqual(latest_comment.file_attachments.count(), 4, 'Comment does not have attachments')

    def test_can_handle_bad_subject_chars(self):
        bad_subject = "Subject \r\n with \n bad chars"
        fixture = msg_fixture.copy()
        fixture['subject'] = bad_subject
        self.handler = EmailHandler(**fixture)
        self.handler.process()
        self.assertLess(len(self.handler.subject), len(bad_subject), 'Bad subject line not sanitised')
