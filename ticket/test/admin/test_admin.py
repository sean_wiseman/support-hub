from django.test import TestCase
from django.contrib.admin.sites import AdminSite
from ticket.models import Ticket
from ticket import admin


class TestTicketAdmin(TestCase):

    fixtures = ['ticket_test_data.json']

    def test_can_use_ticket_description_excerpt(self):
        ticket = Ticket.objects.first()
        site = AdminSite()
        ticket_admin = admin.TicketAdmin(Ticket, site)

        result = ticket_admin.desc_excerpt(ticket)

        self.assertEqual(
            result,
            'Some temp...',
            msg='Should return the first few chars'
        )