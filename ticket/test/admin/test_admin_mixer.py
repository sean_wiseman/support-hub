import pytest
from django.contrib.admin.sites import AdminSite
from mixer.backend.django import mixer
# pytestmark = pytest.mark.django_db
#
# from ticket.models import Ticket
# from .. import admin
#
#
# class TestTicketAdmin:
#     def test_description_excerpt(self):
#         ticket = mixer.blend('ticket.Ticket', description='Some temp desc')
#         site = AdminSite()
#         ticket_admin = admin.TicketAdmin(Ticket, site)
#
#         result = ticket_admin.desc_excerpt(ticket)
#         assert result == 'Some temp...', 'Should return the first few chars'