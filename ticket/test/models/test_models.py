from django.test import TestCase
from django.contrib.auth.models import User
from django.core import mail
from testingUtils.utils import create_ticket
from unittest import skip
from django_mailbox.models import Mailbox, Message
from ticket.models import (
    AdminSubscriber,
    create_ticket_from_email,
    Category,
    Company,
    EmailSubscriber,
    DirtyFieldsGrabberMixin,
    Tag,
    Subscriber,
    Ticket
)
import datetime


class TestTicketModel(TestCase):

    fixtures = ['ticket_test_data.json']
    ticket = None

    def setUp(self):
        self.ticket = create_ticket()

    def test_can_create_ticket(self):
        self.assertEqual(
            len(Ticket.objects.all()),
            4,
            "Actual tickets in db not equal to expected number"
        )
        self.assertEqual(self.ticket.__str__(), '4: test ticket', "Can't create ticket model")

    def test_has_got_correct_status(self):
        self.assertEqual(self.ticket.status, 'open', 'Status is not correct')

    def test_created_today(self):
        self.assertTrue(
            self.ticket.created.date() == datetime.date.today(),
            'Created date time incorrect'
        )

    def test_updated_today(self):
        self.assertTrue(self.ticket.updated.date() == datetime.date.today(), 'Updated date time not correct')

        # save current updated value
        prev_updated = self.ticket.updated
        # trigger a change to auto update the updated date time field
        self.ticket.title = 'New title'
        self.ticket.save()

        self.assertGreater(self.ticket.updated, prev_updated, 'Updated field not changing after model change')

    def test_can_update_duration(self):
        self.assertEqual(self.ticket.duration, 0, 'Duration not zero after init')
        self.ticket.duration = 10
        self.ticket.save()
        self.assertEqual(self.ticket.duration, 10, 'Duration not changed after update')

    def test_can_get_ticket_description_excerpt(self):
        self.assertEqual(
            self.ticket.get_desc_excerpt(chars=9),
            'Some temp...',
            'Should return the first few chars'
        )

    def test_ticket_has_got_correct_type(self):
        self.assertEqual(self.ticket.type, 'issue', 'Category is not correct')

    def test_ticket_has_got_correct_priority(self):
        self.assertEqual(self.ticket.priority, 'medium', 'Priority is not correct')

    def test_ticket_has_got_valid_site_name(self):
        self.assertEqual(self.ticket.site_name, 'test site', 'Site name is not set correctly')

    def test_ticket_has_got_a_valid_category_fk(self):
        ticket = Ticket.objects.first()
        self.assertEqual(ticket.category.name, 'Central Site', 'Unable to view ticket category')

    def test_ticket_has_got_a_valid_company_fk(self):
        ticket = Ticket.objects.first()
        self.assertEqual(ticket.company.name, '2DB', 'Unable to view ticket company')

    def test_ticket_has_got_a_valid_tag_fk(self):
        ticket = Ticket.objects.last()
        self.assertEqual(ticket.tags.first().name, 'gsdm', 'Unable to view ticket tag')

    def test_ticket_has_got_multiple_valid_tags(self):
        ticket = Ticket.objects.last()
        self.assertEqual(ticket.tags.count(), 3, 'Incorrect number of tags on ticket')
        self.assertEqual(ticket.tags.last().name, 'disk space', 'last tag value does not match')

    def test_ticket_has_reported_by_user_fk(self):
        owner = User.objects.get(username='testUser1')
        ticket = Ticket.objects.last()
        self.assertEqual(ticket.reported_by, owner, 'reported by user model is not as expected')

    def test_ticket_has_assigned_to_fk(self):
        assigned_to_user = User.objects.get(username='testUser1')
        ticket = Ticket.objects.first()
        self.assertEqual(
            ticket.assigned_to,
            assigned_to_user,
            'assigned to user model is not as expected'
        )

    def test_can_change_assigned_to_fk(self):
        new_user = User.objects.get(username='testUser3')
        ticket = Ticket.objects.first()
        ticket.assigned_to = new_user
        ticket.save()
        self.assertEqual(
            ticket.assigned_to,
            new_user,
            'Unable to change assigned to user on ticket'
        )

    def test_ticket_has_updated_by_fk(self):
        updated_by_user = User.objects.get(username='testUser2')
        ticket = Ticket.objects.last()
        self.assertEqual(
            ticket.updated_by,
            updated_by_user,
            'updated by user model is not as expected'
        )

    def test_can_changed_updated_by_fk(self):
        new_updated_by_user = User.objects.get(username='testUser3')
        ticket = Ticket.objects.first()
        ticket.updated_by = new_updated_by_user
        ticket.save()
        self.assertEqual(
            ticket.updated_by,
            new_updated_by_user,
            'Unable to change updated by user on ticket'
        )

    def test_can_merge_tickets(self):
        ticket_1 = Ticket.objects.get(id=1)
        ticket_2 = Ticket.objects.get(id=2)
        # check pre merge comment totals
        self.assertEqual(ticket_1.comments.count(), 2)
        self.assertEqual(ticket_2.comments.count(), 0)

        ticket_1.merge_into(ticket_2)

        # check post merge totals
        self.assertEqual(ticket_1.comments.count(), 0)
        self.assertEqual(ticket_2.comments.count(), 2)

    def test_can_get_email_subject_base_str(self):
        ticket = Ticket.objects.first()
        self.assertEqual(
            ticket.get_email_subject_base(),
            'Ticket id: 4 test ticket',
            'Subject base str is incorrect'
        )

    def test_can_get_email_body_base_str(self):
        ticket = Ticket.objects.first()
        self.assertIn(
            'To view and reply to this ticket please visit',
            ticket.get_email_body_base(),
            'Body base str is incorrect'
        )

    def test_can_get_all_admin_subscriber_addesses(self):
        # create some admin subscribers
        for user in User.objects.all():
            AdminSubscriber.objects.create(owner=user)

        self.assertEqual(len(Ticket.get_admin_subscribers()), 3, 'Admin sub count incorrect')

    def test_can_get_all_email_subscriber_addesses(self):
        ticket = Ticket.objects.first()
        for addr in ['test@test.com', 'test2@test.com']:
            EmailSubscriber.objects.create(address=addr, ticket=ticket)
        self.assertEqual(len(ticket.get_email_subscribers()), 2, 'Unable to get EmailSubscriber addresses')

    def test_can_get_all_subscriber_addesses(self):
        ticket = Ticket.objects.first()
        self.assertEqual(len(ticket.get_user_subscribers()), 1, 'Unable to get Subscriber addresses')

    # TODO can get all twodb subscribers

    def test_can_email_subscribers(self):
        ticket = Ticket.objects.first()
        self.assertEqual(ticket.subscribers.count(), 1, 'Number of subs not as expected')

        addresses = ticket.get_user_subscribers() + ticket.get_email_subscribers()

        ticket.send_email_to_subscribers(subject='Test Email', body='Test body content', addresses=addresses)
        self.assertEqual(len(mail.outbox), 1, 'Email not sent')
        self.assertEqual(mail.outbox[0].subject, 'Ticket id: 4 test ticket - Test Email', 'Email subject not correct')
        self.assertIn('Test body content', mail.outbox[0].body, 'Email body is not correct')

    def test_will_email_user_if_ticket_is_assigned_to_them(self):
        ticket = Ticket.objects.get(id=1)
        self.assertEqual(ticket.assigned_to.id, 2, 'Assigned user not as expected')

        new_user = User.objects.get(id=1)
        ticket.assigned_to = new_user
        ticket.save()

        self.assertEqual(len(mail.outbox), 1, 'Email not sent')

    def test_will_email_admin_subscribers_on_ticket_creation(self):
        # create some admin subscribers
        for user in User.objects.all():
            AdminSubscriber.objects.create(owner=user)

        create_ticket()
        self.assertEqual(len(mail.outbox), 1, 'Email not sent on ticket creation to admin subscribers')

        # TODO will email 'email subscribers'
    # TODO will email 'email subscriber' if they are on the send_email_to_subscribers list


class TestTicketsFromEmail(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.mailbox = Mailbox.objects.create(
            name='test',
            uri='testuri'
        )

    def test_will_generate_from_email_comment_after_email_received(self):
        self.assertEqual(Ticket.objects.count(), 3)
        self.message = Message.objects.create(
            mailbox=self.mailbox,
            subject='test subject',
            from_header='Sean Wiseman <swiseman@2db.com>',
            to_header='System Info <systeminfo@2db.co.uk>',
            body='Test email body text'
        )

        self.assertEqual(Message.objects.count(), 1, 'Message not created')

        # manually trigger function that a signal would trigger on Message create
        create_ticket_from_email('', self.message)

        self.assertEqual(Ticket.objects.count(), 4, 'Email did not generate ticket')


class TestCategoryModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_category(self):
        category = Category.objects.first()
        self.assertTrue(category, 'Unable to create Category object')
        self.assertEqual(category.name, 'Central Site', 'name does not match fixture')
        self.assertEqual(category.__str__(), 'Central Site', "__str__ is not correct")


class TestCompanyModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_company(self):
        company = Company.objects.first()
        self.assertTrue(company, 'Unable to create Company object')
        self.assertEqual(company.name, '2DB', 'name does not match fixture')
        self.assertEqual(company.__str__(), '2DB', "__str__ is not correct")


class TestTagModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_tag(self):
        tag = Tag.objects.first()
        self.assertTrue(tag, 'Unable to create Tag object')
        self.assertEqual(tag.name, 'gsdm', 'name does not match fixture')
        self.assertEqual(tag.__str__(), 'gsdm', "__str__ is not correct")


class TestFileAttachmentModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_file_attachment(self):
        # self.fail()
        pass
        # TODO complete test


class TestDirtyFieldsGrabberMixin(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_detect_dirty_field(self):
        ticket = Ticket.objects.first()
        ticket.title = 'new title'
        result = ticket.check_if_field_dirty('title')
        self.assertDictEqual(
            result,
            {
                'changed': True,
                'field': 'title',
                'saved': 'test ticket',
                'current': 'new title',
            },
            'Result not as expected'
        )


class TestAdminSubscriberModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.user = User.objects.get(id=3)
        self.sub = AdminSubscriber.objects.create(owner=self.user)

    def test_can_create_subscriber(self):
        self.assertTrue(self.sub, 'Unable to create AdminSubscriber')
        self.assertEqual(
            self.sub.__str__(),
            'testUser3',
            'str of AdminSubscriber is not as expected'
        )














