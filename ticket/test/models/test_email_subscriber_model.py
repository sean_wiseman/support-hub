from django.test import TestCase
from ticket.models import EmailSubscriber, Ticket


class TestEmailSubscriberModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_email_subscriber(self):
        ticket = Ticket.objects.get(id=1)
        sub = EmailSubscriber.objects.create(
            address='test@test.com',
            ticket=ticket
        )
        self.assertTrue(sub, 'Email subscriber not created')
        self.assertEqual(
            sub.__str__(),
            'test@test.com',
            'str of EmailSubscriber is not as expected'
        )

    def test_can_get_list_of_email_subscribers_from_ticket(self):
        addr_list = ['test@test.com', 'test2@test.com', 'test3@test.com']
        ticket = Ticket.objects.get(id=1)

        for addr in addr_list:
            EmailSubscriber.objects.create(
                address=addr,
                ticket=ticket
            )
        self.assertEqual(
            ticket.email_subscribers.count(),
            3,
            'Email subscribers not created'
        )
