import pytest
from mixer.backend.django import mixer
# mark db to allow writes
# pytestmark = pytest.mark.django_db
#
#
# class TestTicketModelMixer:
#
#     def test_can_create_model(self):
#         ticket = mixer.blend('ticket.Ticket')
#         assert ticket.pk == 1, 'Should create Ticket'
#
#     def test_can_get_ticket_description_excerpt(self):
#         ticket = mixer.blend('ticket.Ticket', description='Some temp desc')
#         result = ticket.get_desc_excerpt(chars=9)
#         assert result == 'Some temp...', 'Should return the first few chars'