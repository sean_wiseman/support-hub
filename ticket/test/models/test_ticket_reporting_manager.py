from django.test import TestCase
from factory.django import DjangoModelFactory
from ticket.models import (
    Category,
    Company,
    Ticket
)
import datetime


class TicketFactory(DjangoModelFactory):
    class Meta:
        model = Ticket


class TestTicketReportingManager(TestCase):

    def setUp(self):
        self.current_month = datetime.datetime.today().month

        company = Company.objects.create(name='test_company0')
        company1 = Company.objects.create(name='test_company1')
        company2 = Company.objects.create(name='test_company2')

        # categories
        category1 = Category.objects.create(name='headend')
        category2 = Category.objects.create(name='receiver')

        # setup issue type tickets
        for i in range(3):
            TicketFactory(company=company, duration=20, category=category1)

        # setup dev type tickets
        for i in range(3):
            TicketFactory(company=company, type='development', duration=20, category=category2)

        for i in range(3):
            TicketFactory(company=company1, duration=10, category=category2)

    def test_can_return_by_type_for_single_company(self):
        by_type_data = Ticket.reporting.by_type(company='test_company0')
        self.assertTrue(by_type_data, 'Unable to get by_type_data')
        self.assertEqual(by_type_data['issue'], 3)
        self.assertEqual(by_type_data['development'], 3)

    def test_can_return_by_type_for_all_companies(self):
        by_type_data = Ticket.reporting.by_type()
        self.assertTrue(by_type_data, 'Unable to get by_type_data for all companies')
        self.assertEqual(len(by_type_data), 3, 'Incorrect number of companies')
        self.assertEqual(by_type_data['test_company0']['issue'], 3)
        self.assertEqual(by_type_data['test_company0']['development'], 3)

    def test_can_return_by_month_for_single_company(self):
        by_month_data = Ticket.reporting.by_month(company='test_company0')
        self.assertTrue(by_month_data, 'Unable to get by_month_data')
        self.assertEqual(len(by_month_data), 12, 'Incorrect number of months')
        self.assertEqual(by_month_data[self.current_month], 6, 'Incorrect number of tickets for September')

    def test_can_return_by_month_for_all_companies(self):
        by_month_data = Ticket.reporting.by_month()
        self.assertTrue(by_month_data, 'Unable to get by_month_data')
        self.assertEqual(len(by_month_data), 3, 'Incorrect number of companies')
        self.assertEqual(by_month_data['test_company0'][self.current_month], 6, 'Incorrect number of tickets for September')

    def test_can_get_duration_data_for_single_company_over_time_span(self):
        by_duration_data = Ticket.reporting.duration_by_month(company='test_company0')
        self.assertTrue(by_duration_data, 'Unable to get by_duration_data')
        self.assertEqual(len(by_duration_data), 12, 'Incorrect number of months')
        self.assertEqual(by_duration_data[self.current_month], 120, 'Incorrect number of minutes for September')

    def test_can_get_duration_data_for_all_companies_over_time_span(self):
        by_duration_data = Ticket.reporting.duration_by_month()
        self.assertTrue(by_duration_data, 'Unable to get by_duration_data')
        self.assertEqual(len(by_duration_data), 3, 'Incorrect number of companies')
        self.assertEqual(by_duration_data['test_company0'][self.current_month], 120, 'Incorrect number of minutes for September')

    def test_can_get_by_category_for_single_company(self):
        by_category_data = Ticket.reporting.by_category(company='test_company0')
        self.assertTrue(by_category_data, 'Unable to get by_duration_data')
        self.assertEqual(len(by_category_data), 2, 'Incorrect number of categories')
        self.assertEqual(by_category_data['headend'], 3, 'Incorrect number of headend tickets for company')
        self.assertEqual(by_category_data['receiver'], 3, 'Incorrect number of receiver tickets for company')

    def test_can_get_by_category_for_all_companies(self):
        by_category_data = Ticket.reporting.by_category()
        self.assertTrue(by_category_data, 'Unable to get by_duration_data')
        self.assertEqual(len(by_category_data), 3, 'Incorrect number of companies')
        self.assertEqual(
            by_category_data['test_company0']['headend'],
            3,
            'Incorrect number of headend tickets for company'
        )
        self.assertEqual(
            by_category_data['test_company0']['receiver'],
            3,
            'Incorrect number of receiver tickets for company'
        )

    # TODO test can get by year for single company
    # TODO test can get by year for all companies

    # TODO test all with future and past years, should expect zero tickets
