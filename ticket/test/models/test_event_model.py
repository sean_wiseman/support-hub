from django.test import TestCase
from django.contrib.auth.models import User
from testingUtils.utils import create_ticket
from ticket.models import (
    Comment,
    Event,
    Ticket
)
import datetime


class TestEventModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.event = Event.objects.create(
            info='some new info',
            owner=User.objects.get(username='testUser1'),
            ticket=Ticket.objects.get(id=1),
            type='created',
        )

    def test_can_create_event(self):
        self.assertTrue(self.event, 'Unable to create Event object')
        self.assertEqual(self.event.type, 'created', 'type does not match fixture')
        self.assertEqual(self.event.info, 'some new info', 'info does not match fixture')
        self.assertEqual(
            self.event.__str__(),
            'created - some new info',
            'str of Event is not as expected'
        )

    def test_created_today(self):
        self.assertTrue(
            self.event.created.date() == datetime.date.today(),
            'Created date time incorrect'
        )

    def test_has_valid_ticket_relationship(self):
        ticket = Ticket.objects.get(id=1)
        self.assertEqual(self.event.ticket, ticket, 'Related Ticket does not match')

    def test_has_valid_owner_relationship(self):
        owner = User.objects.get(username='testUser1')
        self.assertEqual(self.event.owner, owner, 'Related User does not match event owner')

    def test_event_is_generated_when_ticket_is_created(self):
        ticket = create_ticket()
        ticket_events = ticket.events.all()
        event = ticket_events.first()

        self.assertEqual(len(ticket_events), 1, 'No. of events not correct')
        self.assertEqual(event.type, 'created', 'Event type not as expected {0}'.format(event))

    def test_event_is_generated_when_ticket_is_updated(self):
        ticket = create_ticket()
        self.assertEqual(len(ticket.events.all()), 1, 'No. of events not correct')

        # update ticket
        ticket.title = 'some new title'
        ticket.save()

        ticket_events = ticket.events.all()
        self.assertEqual(len(ticket_events), 2, 'No. of events not correct')

        event = ticket_events.first()
        self.assertEqual(event.type, 'updated', 'Event type not as expected')
        self.assertEqual(
            event.info,
            'title: test ticket -> some new title',
            'Event info not as expected'
        )

    def test_event_is_generated_when_comment_added(self):
        ticket = create_ticket()
        user = User.objects.get(username='testUser1')
        self.assertEqual(len(ticket.events.all()), 1, 'No. of events not correct')
        Comment.objects.create(body='test comment', ticket=ticket, owner=user)

        event = ticket.events.first()
        self.assertEqual(event.type, 'comment', 'Event type not as expected')

    def test_event_is_generated_when_a_ticket_is_merged(self):
        ticket = create_ticket()
        ticket2 = create_ticket()

        ticket.merge_into(ticket2)
        event = ticket2.events.first()

        self.assertEqual(ticket2.events.count(), 2, 'Ticket does not have the correct number of tickets')
        self.assertEqual(event.type, 'merge', 'Event type is not as expected')
        self.assertEqual(event.info, 'Ticket id: 4', 'Event info is not as expected')

    # TODO event when subscriber added
    # TODO event when subscriber removed