from django.contrib.auth.models import User
from django.test import TestCase
from django.db.models import ObjectDoesNotExist
from testingUtils.utils import create_ticket, create_user
from ticket.models import (
    Ticket,
    Subscriber
)


class TestSubscriberModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.ticket = Ticket.objects.first()
        self.user = User.objects.get(id=3)
        self.sub = Subscriber.objects.create(owner=self.user, ticket=self.ticket)

    def test_can_create_subscriber(self):
        self.assertTrue(self.sub, 'Unable to create Subscriber')
        self.assertEqual(
            self.sub.__str__(),
            'testUser3',
            'str of Subscriber is not as expected'
        )

    def test_can_create_multiple_subscribers_for_a_ticket(self):
        user = create_user('testUn', 'testPw')
        Subscriber.objects.create(owner=user, ticket=self.ticket)
        self.assertEqual(
            self.ticket.subscribers.count(),
            4,
            'Ticket does not have to correct number of subscribers'
        )

    def test_cannot_add_same_user_twice_as_a_subscriber(self):
        with self.assertRaises(ValueError):
            Subscriber.objects.create(owner=self.user, ticket=self.ticket)

        self.assertEqual(
            self.ticket.subscribers.count(),
            3,
            'Ticket should not allow duplicate subscribers'
        )

    def test_subscriber_will_be_removed_if_user_is_deleted(self):
        self.user.delete()

        # check the ticket no longer exists
        with self.assertRaises(ObjectDoesNotExist):
            User.objects.get(id=self.sub.owner.id)

            # check the subscriber no longer exists after ticket deletion
        with self.assertRaises(ObjectDoesNotExist):
            Subscriber.objects.get(id=self.sub.id)

    def test_subscriber_will_be_removed_if_ticket_is_deleted(self):
        self.ticket.delete()
        # check the ticket no longer exists
        with self.assertRaises(ObjectDoesNotExist):
            Ticket.objects.get(id=self.sub.ticket.id)

            # check the subscriber no longer exists after ticket deletion
        with self.assertRaises(ObjectDoesNotExist):
            Subscriber.objects.get(id=self.sub.id)

    def test_ticket_and_user_still_exist_after_subscriber_removed(self):
        self.sub.delete()
        self.assertTrue(
            Ticket.objects.get(id=self.sub.ticket.id),
            'Ticket deleted when subscriber deleted'
        )
        self.assertTrue(
            User.objects.get(id=self.sub.owner.id),
            'User deleted when subscriber deleted'
        )

    def test_subscriber_is_added_when_ticket_is_created(self):
        ticket = create_ticket()
        reported_by = ticket.reported_by
        subscriber = ticket.subscribers.first()
        self.assertEqual(reported_by.id, subscriber.owner.id, 'subscriber has not been auto added')

    def test_subscriber_is_not_added_when_ticket_is_created_but_has_no_assigned_to_field(self):
        test_data = {
            'assigned_to': None,
            'category': None,
            'company': None,
            'description': 'Some temp desc',
            'reported_by': None,
            'site_name': 'test site',
            'status': 'open',
            'title': 'test ticket',
            'updated_by': None
        }
        ticket = Ticket.objects.create(**test_data)

        self.assertEqual(ticket.subscribers.count(), 0, 'subscriber been added in error')

    def test_subscriber_is_added_when_ticket_is_assigned_to_them(self):
        ticket = Ticket.objects.get(id=3)
        self.assertEqual(ticket.subscribers.count(), 2, 'Ticket subscribers not as expected')

        user = User.objects.get(id=3)
        ticket.assigned_to = user
        ticket.save()
        self.assertEqual(ticket.subscribers.count(), 3, 'Subscriber not added after ticket assignment')




