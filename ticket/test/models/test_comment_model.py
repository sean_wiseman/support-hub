from django.test import TestCase
from django.contrib.auth.models import User
from django.core import mail
from django.core.exceptions import ObjectDoesNotExist
from django_mailbox.models import Mailbox, Message
from ticket.models import (
    Comment,
    Ticket
)


class TestCommentModel(TestCase):
    fixtures = ['ticket_test_data.json']

    def test_can_create_comment(self):
        comment = Comment.objects.first()
        self.assertTrue(comment, 'comment does not match fixture')
        self.assertEqual(
            comment.__str__(),
            'id:5 - ticket:3 - Some comment body te...',
            'str output not as expected'
        )

    def test_has_correct_fields(self):
        comment = Comment.objects.get(id=1)
        owner = User.objects.first()
        ticket = Ticket.objects.first()
        self.assertEqual(comment.body, 'Some comment body text')
        self.assertEqual(comment.ticket, ticket, 'comment ticket relationship is not correct')
        self.assertEqual(comment.owner, owner, 'comment ticket relationship is not correct')
        self.assertFalse(comment.from_email, 'comment from_email flag is not correct')

    def test_will_delete_comments_if_ticket_is_deleted(self):
        comment = Comment.objects.first()
        comment_id = comment.id
        ticket = comment.ticket
        self.assertTrue(ticket, 'Ticket not valid')

        ticket.delete()

        # check that comment no longer exists after ticket fk was deleted
        with self.assertRaises(ObjectDoesNotExist):
            Comment.objects.get(id=comment_id)

    def test_can_add_internal_comment(self):
        ticket = Ticket.objects.first()
        user = User.objects.first()
        comment = Comment(
            body='Some test comment',
            internal=True,
            owner=user,
            ticket=ticket
        )
        self.assertTrue(comment.is_internal(), 'Comment is not an internal comment')

    def test_will_send_emails_to_subscribers_when_comment_added(self):
        owner = User.objects.first()
        ticket = Ticket.objects.first()
        Comment.objects.create(owner=owner, ticket=ticket)
        self.assertEqual(len(mail.outbox), 1, 'Email not sent')

    # TODO will send email to only twodb user for internal comment


class TestCommentsFromEmail(TestCase):
    fixtures = ['ticket_test_data.json']

    def setUp(self):
        self.mailbox = Mailbox.objects.create(
            name='test',
            uri='testuri'
        )

    def test_will_generate_from_email_comment_after_email_received(self):

        # TODO need to decide how to link incoming email to ticket

        self.message = Message.objects.create(
            mailbox=self.mailbox,
            subject='test subject',
            from_header='Sean Wiseman <swiseman@2db.com>',
            to_header='System Info <systeminfo@2db.co.uk>',
            body='Test email body text'
        )

        self.assertEqual(Message.objects.count(), 1, 'Message not created')




