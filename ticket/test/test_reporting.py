from django.test import TestCase
from ticket.models import Company
from ticket.reporting import ReportGenerator
import datetime


class TestReportGenerator(TestCase):
    fixtures = ['ticket_test_data.json']

    year = datetime.date(2017, 9, 7).year

    def setUp(self):
        self.company = Company.objects.first()
        self.gen = ReportGenerator(
            company=self.company,
            year=self.year
        )

    def test_has_valid_attributes_on_init(self):
        self.assertEqual(self.gen.company, self.company)
        self.assertEqual(self.gen.company.name, '2DB')
        self.assertEqual(self.gen.year, self.year)

    # def test_from_date_is_in_the_past_if_not_provided(self):
    #     gen = ReportGenerator()
    #     self.assertTrue(gen.from_date < datetime.date.today())

    # def test_to_date_is_today_if_not_provided(self):
    #     gen = ReportGenerator()
    #     self.assertEqual(gen.to_date, datetime.date.today())

    def test_can_get_queryset(self):
        self.assertEqual(self.gen.get_queryset().count(), 2, 'Unable to get queryset')

    def test_will_return_all_tickets_is_qs_if_no_company_is_provided(self):
        gen = ReportGenerator(year=self.year)
        self.assertEqual(gen.get_queryset().count(), 3, 'Unable to get queryset')

    def test_will_return_all_tickets_count_total_by_customer(self):
        gen = ReportGenerator(year=self.year)
        self.assertEqual(
            gen.total_ticket_count(),
            [{'2DB': 2}, {'some_company': 1}],
            'Unable to get ticket counts for all companies'
        )

    def test_will_return_tickets_count_total_for_customer(self):
        self.assertEqual(
            self.gen.total_ticket_count(),
            2,
            'Unable to get ticket count'
        )

    def test_will_return_tickets_by_month_table_data_for_one_company(self):
        data = self.gen.tickets_by_month_data()
        self.assertTrue(data, 'Unable to get ticket by month data')
        self.assertEqual(len(data[0]), 2, 'Data headers not correct')
        self.assertEqual(data[3][1], 2, 'No ticket count data returned for March from fixtures')

    def test_will_return_tickets_by_month_table_data_for_all_companies(self):
        gen = ReportGenerator(year=self.year)
        data = gen.tickets_by_month_data()
        self.assertTrue(data, 'Unable to get ticket by month data')
        self.assertEqual(len(data[0]), 3, 'Data headers not correct')
        self.assertEqual(data[3][1], 2, 'No ticket count data returned for expected month')

    # def test_will_return_tickets_by_type_table_data_for_one_company(self):
    #     data = self.gen.tickets_by_type_data()
    #     self.assertTrue(data, 'Unable to get ticket by type data')
    #     self.assertEqual(len(data), 9, 'Data headers not correct')
    #     self.assertEqual(data[4][1], 1, 'No ticket count data returned for issue type fixtures')



# TODO test ChartFactory