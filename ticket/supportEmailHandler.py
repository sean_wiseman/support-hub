from django.db.models.fields.files import FileField
from django.conf import settings
from ticket.models import Comment, EmailSubscriber, FileAttachment, Ticket
from typing import List
import logging
import re
from email.utils import parseaddr


class EmailHandler:
    """
    Takes a loose email data structure then converts and generates support hub
    tickets and comments based on existing or new tickets
    """

    auto_reply_base_text = [
        'Thank you, the 2DB support team has received your mail.\n\n',
        'These emails are monitored during normal workday hours (Mon-Fri 9am-5:30pm).\n',
        "Outside these hours, if it can't wait, please call us on (0)1206 396640.\n"
    ]

    def __init__(self, attachments: List[FileField], body: str, sender: str, cc_list: str, subject: str, msg_id: str) -> None:
        """
        :param body:
        :param sender:
        :param cc_list:
        :param subject:
        :param msg_id:

        Usage:
        >>> msg_fixture = {
            'body': 'Some body text',
            'cc_list': 'Some Guy <someguy@test.com> Some OtherGuy <someotherguy@test.com>',
            'msg_id': '<CAN9qYMWddFcfGSNHptCPSGai4mLNHO0_tOde1_AYYJTz7qD7+w@mail.gmail.com>',
            'sender': 'Sean Wiseman <swiseman@2db.com>',
            'subject': 'Re: Ticket id: 1 some other subject'
        }
        >>> handler = EmailHandler(**msg_fixture)
        >>> handler.process()

        """
        self.attachments = attachments
        self.body = body
        self.cc_list = self.parse_cc_list(cc_list)
        self.msg_id = msg_id
        self.logger = logging.getLogger('ticket')
        self.sender = self.find_address(sender)
        trans = str.maketrans('', '', '\r\n')
        self.subject = subject.translate(trans)  # Remove any illegal header characters
        self.ticket_id = self.find_ticket_id(self.subject)

    def add_comment_to_existing_ticket(self, ticket: Ticket, body: str = '', notify: bool = True) -> None:
        comment = Comment(
            body=body,
            from_email=True,
            sender=self.sender,
            ticket=ticket
        )
        comment.save(notify=notify)

        # change updated by to None to remove previous updated by user
        ticket.updated_by = None
        ticket.save()

        if self.attachments:
            for attch in self.attachments:
                FileAttachment.objects.create(
                    comment=comment,
                    raw_file=attch.document
                )

    def add_email_subscribers(self, ticket: Ticket) -> None:
        """Take email addresses from sender and cc list,
        If the address is not already in the ticket's email subscribers
        then create and add email subscriber
        """
        addresses = self.cc_list.copy()
        addresses.append(self.sender)

        # Exclude existing addresses
        exclude_addresses = [sub.address for sub in EmailSubscriber.objects.filter(ticket=ticket)]
        if settings.EMAIL_EXCLUDE_LIST:  # Exclude main support addresses e.g. support@2db.co.uk
            exclude_addresses += settings.EMAIL_EXCLUDE_LIST

        for address in addresses:
            if address not in exclude_addresses:
                EmailSubscriber.objects.create(
                    address=address,
                    ticket=ticket
                )

    def create_ticket(self) -> Ticket:
        return Ticket.objects.create(
            title=self.subject,
            description='{0} \n\n Sender: {1}'.format(self.body, self.sender)
        )

    @staticmethod
    def find_address(target_str: str) -> str:
        """Will find a string between < & > chars.
        >>> sender = 'Sean Wiseman <swiseman@2db.com>'
        >>> self.find_address(sender)
        swiseman@2db.com
        """
        email = parseaddr(target_str)[1]
        # Check this looks like a 'real' address and not just an alias
        if '@' in email:
            return email
        else:
            return False

    @staticmethod
    def find_ticket_id(subject: str) -> int:
        """Will find a ticket id if the string is in a certain format.
        >>> subject = 'Re: Ticket id: 1010111 some other subject'
        >>> self.find_ticket_id(subject)
        1010111
        """
        ro = re.compile(r'(Ticket id:\s)([0-9]*)')
        mo = ro.search(subject)
        if mo:
            result = mo.group()
            ticket_id = int(result.split(':')[1])
            return ticket_id
        return None

    def get_email_body_for_comment(self) -> str:
        return 'cc: {cc_list} \n\n{body}'.format(body=self.body, cc_list=self.cc_list)

    @property
    def has_ticket_ref(self) -> bool:
        if self.ticket_id is not None:
            return True
        else:
            return False

    def parse_cc_list(self, raw_list: List[str]):
        """Convert list of raw strings into email addresses
        :param raw_list:
        :return:
        """
        if raw_list:
            email_list = raw_list.split(',')
            return [parseaddr(email)[1] for email in email_list]
        else:
            return []

    def process(self) -> None:
        """Process email data based on it being related to an existing ticket or not.
        :return:
        """
        if self.has_ticket_ref:
            ticket = Ticket.objects.get(id=self.ticket_id)
            self.add_email_subscribers(ticket)
            self.add_comment_to_existing_ticket(ticket, body=self.get_email_body_for_comment())
        else:
            ticket = self.create_ticket()
            self.add_email_subscribers(ticket)
            self.send_auto_reply_email(ticket)
            # add attachments to ticket as comment if present
            if self.attachments:
                self.add_comment_to_existing_ticket(ticket, body='Attachment(s) from creation email', notify=False)

    def send_auto_reply_email(self, ticket: Ticket) -> None:
        """Send reply email to sender of newly created ticket
        :param ticket:
        :return:
        """
        addresses = ticket.get_user_subscribers() + ticket.get_email_subscribers()
        ticket.send_email_to_subscribers(
            subject='Auto Reply',
            body=''.join(self.auto_reply_base_text),
            addresses=addresses
        )



"""


class MessageAttachment(models.Model):
    message = models.ForeignKey(
        Message,
        related_name='attachments',
        null=True,
        blank=True,
        verbose_name=_('Message'),
    )

    headers = models.TextField(
        _(u'Headers'),
        null=True,
        blank=True,
    )

    document = models.FileField(
        _(u'Document'),
        upload_to=utils.get_attachment_save_path,
    )

"""