from supportHub.celery import app
from django_mailbox.models import Mailbox


@app.task(name="get_mail")
def fetch_mail():
    mailbox = Mailbox.objects.first()
    mailbox.get_new_mail()
