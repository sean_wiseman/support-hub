import django_filters
from ticket.models import Ticket


class TicketFilter(django_filters.FilterSet):

    class Meta:
        model = Ticket
        fields = [
            'assigned_to',
            'category',
            'company',
            'priority',
            'reported_by',
            'status',
            'type',
            'updated_by'
        ]


class TicketSearchFilter(django_filters.FilterSet):
    comments = django_filters.CharFilter(name='comments', lookup_expr='body__icontains')
    created = django_filters.DateFromToRangeFilter()
    description = django_filters.CharFilter(name='description', lookup_expr='icontains')
    title = django_filters.CharFilter(name='title', lookup_expr='icontains')
    updated = django_filters.DateFromToRangeFilter()

    class Meta:
        model = Ticket
        fields = [
            'assigned_to',
            'category',
            'company',
            'comments',
            'created',
            'description',
            'priority',
            'reported_by',
            'status',
            'title',
            'tags',
            'type',
            'updated_by',
            'updated'
        ]
