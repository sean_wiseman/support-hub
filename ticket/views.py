from braces.views import LoginRequiredMixin
from django import http
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.generic import FormView, RedirectView, View
from django.views.generic.edit import CreateView, DeleteView, FormMixin, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django_filters.views import FilterView
from ticket.filters import TicketFilter, TicketSearchFilter
from ticket.models import (
    Comment,
    Company,
    EmailSubscriber,
    Subscriber,
    Tag,
    Ticket
)
from ticket.forms import (
    CommentCreateForm,
    EmailSubscriberForm,
    SubscriberForm,
    TagCreateAndAddForm,
    TagRemoveForm,
    TicketForm,
    TicketUpdateForm,
    TicketMergeForm,
    TicketMultiMergeForm,
    TicketReportingForm
)
from ticket.reporting import ReportGenerator
import logging


def parse_params(request):
    return {p: v[0] for p, v in dict(request.GET).items()}


class ListOrderByMixin(object):
    order_by = None

    def get_context_data(self, **kwargs):
        # this is called just before rendering the response
        kwargs = super(ListOrderByMixin, self).get_context_data(**kwargs)
        # add order_by to context so custom ordering can persist through pagination
        if self.order_by:
            kwargs['order_by'] = self.order_by
        return kwargs

    def get_order_by(self):
        """Custom func to process user order_by value in view
        Will also handle reversing of the same value e.g.
        id -> -id
        """
        new_order_by = self.request.GET.get('order_by', None)

        # check if order_by is stored in session
        session_order_by = self.request.session.get('order_by', None)

        if new_order_by and session_order_by:
            # check if new order_by is the same as stored
            if new_order_by == session_order_by:
                if '-' in new_order_by:
                    new_order_by = new_order_by.strip('-')
                else:
                    new_order_by = '-' + new_order_by

        order_by = new_order_by
        self.request.session['order_by'] = order_by

        # create an instance attribute reference for other functions to use during request
        self.order_by = order_by

        return order_by

    def get_ordering(self):
        # this is called before get_context_data
        ordering = self.get_order_by()
        return ordering


class FilterByCompanyMixin(object):

    def filter_by_company(self, qs):
        try:
            if self.request.user.profile.is_twodb_user():
                return qs
            else:
                company = Company.objects.get(name=self.request.user.profile.company)
                return qs.filter(company=company)
        except ObjectDoesNotExist:
            return []


class CommentCreateView(LoginRequiredMixin, FormView):
    form_class = CommentCreateForm
    template_name = 'comment_form.html'
    ticket_id = None

    def form_valid(self, form):
        self.ticket_id = form.cleaned_data['ticket']
        valid_form = super(CommentCreateView, self).form_valid(form)
        form.save()
        return valid_form

    def get_success_url(self):
        return reverse('ticket:update_ticket', kwargs={'id': self.ticket_id})


class CommentDeleteView(LoginRequiredMixin, DeleteView):
    model = Comment
    pk_url_kwarg = 'id'

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.ticket_id = self.object.ticket.id
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        return reverse('ticket:update_ticket', kwargs={'id': self.ticket_id})


class EmailSubscriberCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = EmailSubscriber
    form_class = EmailSubscriberForm
    success_message = 'Email Subscriber added'
    template_name = 'email_subscriber_form.html'
    ticket_id = None

    def form_valid(self, form):
        self.ticket_id = form.instance.ticket.id
        valid_form = super(EmailSubscriberCreateView, self).form_valid(form)
        return valid_form

    def get_success_url(self):
        return reverse('ticket:subscriber_list', kwargs={'id': self.ticket_id})


class EmailSubscriberDeleteView(LoginRequiredMixin, DeleteView):
    model = EmailSubscriber
    pk_url_kwarg = 'id'

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.ticket_id = self.object.ticket.id
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        messages.success(self.request, 'Email subscriber removed')
        return reverse('ticket:subscriber_list', kwargs={'id': self.ticket_id})


class SubscriberCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Subscriber
    form_class = SubscriberForm
    success_message = 'Subscriber added'
    ticket_id = None

    def form_valid(self, form):
        self.ticket_id = form.instance.ticket.id
        try:
            valid_form = super(SubscriberCreateView, self).form_valid(form)
            return valid_form
        except ValueError as err:
            messages.error(self.request, err)
            return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('ticket:subscriber_list', kwargs={'id': self.ticket_id})


class SubscriberAddView(LoginRequiredMixin, View):
    """Allows Subscriber additions via GET requests, used with add subscriber buttons"""

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id', None)
        ticket_id = kwargs.get('ticket_id', None)
        redirect_url = reverse('ticket:ticket_list')

        form = SubscriberForm({'owner': int(user_id), 'ticket': int(ticket_id)})

        if form.is_valid():
            subscriber = form.save()
            if subscriber:
                messages.success(request, 'Subscriber Added')
            redirect_url = reverse('ticket:update_ticket', kwargs={'id': ticket_id})
        else:
            messages.error(request, 'User Id or Ticket Id not valid')

        # try to find previous page or just redirect to ticket update view
        # redirect_url = request.META.get('HTTP_REFERER', reverse('ticket:update_ticket', kwargs={'id': ticket_id}))

        return redirect(redirect_url)


class SubscriberDeleteView(LoginRequiredMixin, View):
    """Allows Subscriber additions via GET requests"""

    http_method_names = [u'get', u'delete', u'head', u'options', u'trace']

    def get(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id', None)
        ticket_id = kwargs.get('ticket_id', None)
        redirect_url = reverse('ticket:ticket_list')
        r_url = request.GET.get('redirect_url', None)

        try:
            ticket = Ticket.objects.get(id=ticket_id)
        except ObjectDoesNotExist:
            ticket = None

        if ticket:
            for sub in ticket.subscribers.all():
                if sub.owner.id == int(user_id):
                    sub.delete()
                    messages.success(request, 'Subscriber Removed')
            if r_url:
                redirect_url = r_url
            else:
                redirect_url = reverse('ticket:update_ticket', kwargs={'id': ticket_id})
        else:
            messages.error(request, 'User Id or Ticket Id not valid')

        return redirect(redirect_url)


class SubscriberListView(LoginRequiredMixin, DetailView):
    model = Ticket
    pk_url_kwarg = 'id'
    template_name = 'subscriber_list.html'

    def get_context_data(self, **kwargs):
        """Insert x2 forms into the context dict"""

        if 'form' not in kwargs:
            kwargs['form'] = SubscriberForm()

        ticket = self.get_object()

        kwargs['subscriber_list'] = ticket.subscribers.all()

        return super(SubscriberListView, self).get_context_data(**kwargs)


class TagCreateAndAddView(LoginRequiredMixin, FormView):
    form_class = TagCreateAndAddForm
    template_name = 'tag_create.html'
    ticket_id = None
    failure_url = reverse_lazy('ticket:ticket_list')

    def get_success_url(self):
        return reverse('ticket:update_ticket', kwargs={'id': self.ticket_id})

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form()
        if form.is_valid():
            self.ticket_id = form.cleaned_data.get('ticket_id', None)
            form.save()
            messages.success(self.request, 'Tag(s) Added')
            return self.form_valid(form)
        else:
            messages.error(self.request, 'Tag(s) Not Added')
            return HttpResponseRedirect(self.failure_url)


class TagRemoveFromTicketView(LoginRequiredMixin, View):
    """Removes tag from a given ticket's many to many field"""

    def get(self, request, *args, **kwargs):
        tag_id = kwargs.get('tag_id', None)
        ticket_id = kwargs.get('ticket_id', None)
        redirect_url = reverse('ticket:ticket_list')

        try:
            form = TagRemoveForm({'tag_id': int(tag_id), 'ticket_id': int(ticket_id)})

            if form.is_valid():
                ticket = Ticket.objects.get(id=ticket_id)
                tag = Tag.objects.get(id=tag_id)
                ticket.tags.remove(tag)
                messages.success(request, 'Tag Removed')
                redirect_url = reverse('ticket:update_ticket', kwargs={'id': ticket_id})
        except ValueError:
            messages.error(request, 'Tag Id or Ticket Id not valid')

        return redirect(redirect_url)


class TicketCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Ticket
    form_class = TicketForm
    template_name = 'ticket_create.html'
    success_message = 'Ticket created'

    def form_valid(self, form):
        form.instance.reported_by = self.request.user
        form.instance.updated_by = self.request.user
        valid_form = super(TicketCreateView, self).form_valid(form)
        # messages.success(self.request, "Ticket created")
        return valid_form

    def get_success_url(self):
        return reverse('ticket:ticket_list')


class TicketDeleteView(LoginRequiredMixin, DeleteView):
    model = Ticket
    pk_url_kwarg = 'id'
    template_name = 'ticket_delete.html'

    def get_success_url(self):
        return reverse('ticket:ticket_list')


class TicketListView(LoginRequiredMixin, ListOrderByMixin, FilterByCompanyMixin, FilterView):
    model = Ticket
    context_object_name = 'ticket_list'
    template_name = 'ticket_list.html'
    logger = logging.getLogger('ticket')
    paginate_by = 18

    filterset_class = TicketFilter

    def dispatch(self, request, *args, **kwargs):
        # self.logger.debug('test')

        # self.logger.critical(
        #     '%(err_type)s: %(err)s - unable to create target',
        #     {
        #         'err_type': type(err),
        #         'err': err
        #     }
        # )
        # messages.success(self.request, "Debug")

        return super(TicketListView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def filter_closed_tickets(request, qs):
        """If status filter field != 'Closed', filter out closed ticket from view"""
        request_params = parse_params(request)
        if request_params.get('status', None) != 'Closed' and qs:
            return qs.exclude(status='Closed')
        else:
            return qs

    def get(self, request, *args, **kwargs):
        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filter_closed_tickets(request, self.filter_by_company(self.filterset.qs))

        context = self.get_context_data(
            filter=self.filterset,
            object_list=self.object_list
        )
        return self.render_to_response(context)


class TicketQuickSearchView(LoginRequiredMixin, RedirectView):

    def get(self, request, *args, **kwargs):
        url = self.get_redirect_url(**kwargs)

        if url:
            return http.HttpResponseRedirect(url)
        else:
            messages.error(self.request, 'No Ticket found')
            return http.HttpResponseRedirect(self.get_fallback_url())

    @staticmethod
    def get_fallback_url():
        return reverse('ticket:ticket_list')

    def get_redirect_url(self, *args, **kwargs):
        try:
            ticket_id = int(self.request.GET.get('ticket_id', None))
        except ValueError:
            messages.error(self.request, 'Please enter a valid Ticket ID')
            return self.get_fallback_url()

        if Ticket.objects.filter(id=ticket_id).exists():
            return reverse('ticket:update_ticket', kwargs={'id': ticket_id})


class TicketMergeView(LoginRequiredMixin, FormView):
    form_class = TicketMergeForm
    template_name = 'ticket_merge.html'

    # TODO check if admin

    def merge_tickets(self, from_ticket_id, to_ticket_id):
        from_ticket = Ticket.objects.get(id=from_ticket_id)
        to_ticket = Ticket.objects.get(id=to_ticket_id)
        from_ticket.merge_into(to_ticket)
        from_ticket.delete()

    def form_valid(self, form):
        """
        If the form is valid, merge tickets then redirect to the supplied URL.
        """

        self.from_ticket_id = form.cleaned_data['from_ticket']
        self.to_ticket_id = form.cleaned_data['to_ticket']

        # perform ticket merge
        self.merge_tickets(self.from_ticket_id, self.to_ticket_id)

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()

        # if id passed in url (which it should) assign back to context to pass to form
        ticket_id = self.request.resolver_match.kwargs.get('id', None)
        if ticket_id:
            kwargs['ticket_id'] = ticket_id

        return super(TicketMergeView, self).get_context_data(**kwargs)

    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        return reverse('ticket:update_ticket', kwargs={'id': self.to_ticket_id})


@login_required
def ticket_multi_select_view(request):
    # TODO check if admin
    if request.method == 'POST':
        post_data = dict(request.POST)
        ticket_ids = [ticket_id for ticket_id in post_data.get('tickets_selected', [])]

        if len(ticket_ids):
            request.session['selected_tickets'] = ticket_ids

            return render(request, 'ticket_multi_select.html', {'selected_tickets': ticket_ids})
        else:
            messages.error(request, 'No tickets were selected')

    return redirect(reverse('ticket:ticket_list'))


def ticket_multi_delete_view(request, *args, **kwargs):
    # TODO check if admin
    if request.method == 'GET':
        # check selected tickets are present in session or kwargs
        try:
            selected_tickets = request.session.get('selected_tickets', None)
        except AttributeError:
            selected_tickets = kwargs.get('selected_tickets', None)

        if selected_tickets:
            for ticket_id in selected_tickets:
                from_ticket = Ticket.objects.get(id=int(ticket_id))
                from_ticket.delete()
            messages.add_message(request, messages.SUCCESS, 'Tickets deleted successfully', fail_silently=True)
        else:
            messages.add_message(request, messages.ERROR, 'No tickets were selected', fail_silently=True)

    return redirect(reverse('ticket:ticket_list'))


@login_required
def ticket_multi_merge_view(request, *args, **kwargs):
    # TODO check if admin
    if request.method == 'POST':
        # check selected tickets are present in session or kwargs
        try:
            selected_tickets = request.session.get('selected_tickets', None)
        except AttributeError:
            selected_tickets = kwargs.get('selected_tickets', None)

        if selected_tickets:
            merge_form = TicketMultiMergeForm(request.POST or None)
            if merge_form.is_valid():
                to_ticket = Ticket.objects.get(id=int(merge_form['to_ticket'].value()))
                for ticket_id in selected_tickets:
                    from_ticket = Ticket.objects.get(id=int(ticket_id))
                    from_ticket.merge_into(to_ticket)
                    from_ticket.delete()
                messages.add_message(request, messages.SUCCESS, 'Tickets merged successfully', fail_silently=True)
        else:
            messages.add_message(request, messages.ERROR, 'No tickets were selected', fail_silently=True)

    return redirect(reverse('ticket:ticket_list'))


class TicketReportingView(LoginRequiredMixin, FormView):
    form_class = TicketReportingForm
    template_name = 'ticket_reporting.html'
    report = {}

    def form_valid(self, form):
        """
        If the form is valid, merge tickets then redirect to the supplied URL.
        """

        company = form.cleaned_data['company']
        year = form.cleaned_data['year']

        context = {
            'form': form,
            'report': ReportGenerator(company, year).get_data()
        }

        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()

        return super(TicketReportingView, self).get_context_data(**kwargs)

    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        return reverse('ticket:reporting')


class TicketSearchView(LoginRequiredMixin, ListOrderByMixin, FilterByCompanyMixin, FilterView):
    model = Ticket
    context_object_name = 'ticket_list'
    template_name = 'ticket_search.html'
    logger = logging.getLogger('ticket')
    paginate_by = 15

    filterset_class = TicketSearchFilter

    def dispatch(self, request, *args, **kwargs):
        # self.logger.debug('test')
        return super(TicketSearchView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        filterset_class = self.get_filterset_class()
        self.filterset = self.get_filterset(filterset_class)
        self.object_list = self.filter_by_company(self.filterset.qs)

        # pass params back to template so they can be used to keep params in inputs between calls
        request_params = parse_params(request)

        context = self.get_context_data(
            filter=self.filterset,
            object_list=self.object_list,
            request_params=request_params,
            total_qs_count=self.filterset.qs.count()
        )
        return self.render_to_response(context)


class TicketUpdateView(LoginRequiredMixin, UpdateView):
    model = Ticket
    form_class = TicketUpdateForm
    pk_url_kwarg = 'id'
    template_name = 'ticket_update.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        valid_form = super(TicketUpdateView, self).form_valid(form)
        return valid_form

    def get(self, request, *args, **kwargs):
        ticket = self.get_object()

        if self.request.user.profile.is_twodb_user() or self.request.user.profile.company == ticket.company.name:
            return super(TicketUpdateView, self).get(request, *args, **kwargs)
        else:
            messages.error(self.request, 'You do not have permission to view ticket {}'.format(kwargs.get('id', None)))
            return HttpResponseRedirect(reverse('ticket:ticket_list'))

    def get_context_data(self, **kwargs):
        """Insert x2 forms into the context dict"""
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()

        if 'comment_form' not in kwargs:
            kwargs['comment_form'] = CommentCreateForm()

        if 'tag_create_form' not in kwargs:
            kwargs['tag_create_form'] = TagCreateAndAddForm()

        kwargs['user_subscribed'] = self.get_object().has_subscriber(self.request.user.id)

        return super(TicketUpdateView, self).get_context_data(**kwargs)
