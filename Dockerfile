FROM python:3.6.2-slim
MAINTAINER Sean Wiseman <sean.wiseman@hotmail.co.uk>

ENV INSTALL_PATH /support-hub
RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD gunicorn -b 0.0.0.0:8000 supportHub.wsgi