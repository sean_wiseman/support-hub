from django.contrib.auth.models import User
from authentication.models import Profile
from ticket.models import Category, Company, Ticket


test_un = 'testUserTemp'
test_pw = 'somepassword1'


def create_ticket() -> Ticket:
    """Assumes ticket_test_data.json fixtures have been loaded"""
    user = User.objects.first()
    test_data = {
        'assigned_to': user,
        'category': Category.objects.first(),
        'company': Company.objects.first(),
        'description': 'Some temp desc',
        'reported_by': user,
        'site_name': 'test site',
        'status': 'open',
        'title': 'test ticket',
        'updated_by': user
    }
    return Ticket.objects.create(**test_data)


def create_user(un='', pw='', company='2DB', email='test@test.com') -> User:
    user = User.objects.create(username=un, password=pw, email=email)
    user.set_password(pw)
    user.save()

    profile = Profile.objects.create(user=user, company=company)
    profile.save()
    return user


def resolves_to_correct_func(resolver, expected_func) -> bool:
    return resolver.func.__name__ == expected_func.as_view().__name__
