import os
from celery import Celery
from celery.schedules import crontab


# set the default Django settings module for the 'celery' program
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'supportHub.settings')

app = Celery('supportHub')

app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'check-for-email-every-minute': {
        'task': 'get_mail',
        'schedule': crontab()  # once a minute
    }
}


# @app.task(bind=True)
# def debug_task(self):
#     print('Request: {0!r}'.format(self.request))
