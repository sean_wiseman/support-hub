from django.test import TestCase
from django.contrib.auth.models import User
from testingUtils.utils import create_user
from ticket.models import Company


class TestProfileModel(TestCase):
    # fixtures = ['user_test_data.json']
    twodb_name = '2DB'
    other_company_name = 'some_other_company'

    def setUp(self):
        Company.objects.create(name=self.twodb_name)
        Company.objects.create(name=self.other_company_name)
        create_user('testUser1', 'testpw')
        create_user('testUser2', 'testpw')

    def test_user_has_profile_on_create(self):
        user = User.objects.first()
        profile = user.profile
        self.assertTrue(profile, 'User does not have a profile after create')
        self.assertEqual(profile.__str__(), '2DB', 'str not as expected')

    def test_if_can_detect_if_user_is_a_twodb_user(self):
        user = User.objects.first()
        profile = user.profile
        profile.company = self.twodb_name
        profile.save()

        self.assertTrue(user.profile.company, 'Profile company is not valid')
        self.assertEqual(user.profile.company, self.twodb_name, 'Profile company name not correct')
        self.assertTrue(user.profile.is_twodb_user(), 'Unable to detect 2DB user')

    def test_if_can_detect_if_user_is_not_a_twodb_user(self):
        user = User.objects.first()
        profile = user.profile
        profile.company = self.other_company_name
        profile.save()

        self.assertTrue(user.profile.company, 'Profile company is not valid')
        self.assertEqual(user.profile.company, self.other_company_name, 'Profile company name not correct')
        self.assertFalse(user.profile.is_twodb_user(), 'Unable to detect if not a 2DB user')
