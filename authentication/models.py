from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.db.utils import OperationalError, ProgrammingError
from django.dispatch import receiver

from ticket.models import Company


try:
    company_names = [company.name for company in Company.objects.all()]
except (OperationalError, ProgrammingError):
    company_names = ['no companies']

COMPANY_CHOICES = zip(company_names, company_names)
DEFAULT_COMPANY_CHOICE = 'Not Set'


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company = models.CharField(
        max_length=100,
        choices=COMPANY_CHOICES,
        default=DEFAULT_COMPANY_CHOICE,
        null=True,
        blank=True
    )

    def __str__(self):
        return '{0}'.format(self.company)

    def is_twodb_user(self) -> bool:
        return self.company.upper() == '2DB'


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         if not Profile.objects.filter(user=instance):
#             Profile.objects.create(user=instance)
