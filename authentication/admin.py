from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from authentication.models import Profile


class ProfileInline(admin.StackedInline):
    model = Profile


UserAdmin.list_display = ('username', 'email', 'first_name', 'last_name', 'is_active', 'date_joined', 'is_staff', 'profile')
UserAdmin.inlines = (ProfileInline,)
