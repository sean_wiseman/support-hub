2DB Support Hub
==================================================

Ticketing system for internal and customer based faults and queries.


Preparing a Deployment Server
----------------------------
Target server will preferably be an Ubuntu instance => 14.0.4

- Update system

        $ sudo apt-get update

- Install Docker

https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-docker-ce

* if you have issues try:
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"

- Install Docker Compose

        $ sudo apt-get -y install python-pip
        
        $ sudo pip install docker-compose
        
Installing and starting the App via Docker Compose
-----------------------------------
        * Build app
        $ cd /support-hub
        $ sudo docker-compose up --build
        
        * Create superuser and 2DB customer
        $ sudo docker exec -it supporthub_website_1 bash
        $ cd support-hub
        $ python manage.py createsuperuser
        
        * Create company
        - Log into support hub admin (/admin)
        - Under Tickets, select add Company
        - Create '2DB' Company
        
        * Restart server
        $ sudo docker-compose down
        $ sudo docker-compose up
        
        * Setup mailbox
        - Log into support hub admin (/admin)
        - Under Django mailbow, select add Mailbox using the following details:
        
            Name: support mailbox
            URI: pop3+ssl://support@2db.co.uk:2dbS3cur1ty@smtp.gmail.com
            From Email: support@2db.co.uk
            Active: True